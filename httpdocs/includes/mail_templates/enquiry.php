<?
$msg = "The following enquiry has been sent via the RMG Lettings website (www.rmglettings.co.uk):\r\n\r\n";

$msg .= "Title: ".$_REQUEST['title']."\r\n";
$msg .= "First name(s): ".$_REQUEST['first_name']."\r\n";
$msg .= "Surname: ".$_REQUEST['last_name']."\r\n";
$msg .= "Email: ".$_REQUEST['email']."\r\n";
$msg .= "Telephone no.: ".$_REQUEST['tel']."\r\n";
$msg .= "Which development?: ".$_REQUEST['development']."\r\n";
$msg .= "Current address: ".$_REQUEST['address']."\r\n\r\n";

$msg .= "Move-in date: ".$_REQUEST['move_in_date']."\r\n";
$msg .= "Move-out date: ".$_REQUEST['move_out_date']."\r\n";
$msg .= "Preferred method of contact: ".$_REQUEST['method']."\r\n";
$msg .= "Comments/questions: ".$_REQUEST['comments']."\r\n";

?>