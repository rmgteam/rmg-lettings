<?php
require_once 'utils.php';
Global $UTILS_TEL_LETTINGS_MAIN;
// Send enquiry
if($_REQUEST['a'] == "s"){

	require("library/field.class.php");

	$field = new field;
	$save_result = true;

	// Check mandatory fields
	if($_REQUEST['title'] == ""){$save_result = "Please complete all required fields marked with (*).";}
	if($_REQUEST['first_name'] == ""){$save_result = "Please complete all required fields marked with (*).";}
	if($_REQUEST['last_name'] == ""){$save_result = "Please complete all required fields marked with (*).";}
	if($_REQUEST['email'] == ""){$save_result = "Please complete all required fields marked with (*).";}
	if($_REQUEST['tel'] == ""){$save_result = "Please complete all required fields marked with (*).";}
	//if($_REQUEST['development'] == ""){$save_result = "Please complete all required fields marked with (*).";}
	if($_REQUEST['address'] == ""){$save_result = "Please complete all required fields marked with (*).";}
	if($_REQUEST['move_in_date'] == ""){$save_result = "Please complete all required fields marked with (*).";}
	if($_REQUEST['move_out_date'] == ""){$save_result = "Please complete all required fields marked with (*).";}
	if($_REQUEST['method'] == ""){$save_result = "Please complete all required fields marked with (*).";}

	// Specific checks
	if($save_result === true){
		if(!$field->is_valid_email($_REQUEST['email'])){$save_result = "Please provide a valid email address.";}
	}

	// Send enquiry to RMG
	if($save_result === true){

		require("includes/mail_templates/enquiry.php");
		@mail("info@rmglettings.co.uk","Enquiry from RMG Lettings website...", $msg, "From:".$_REQUEST['email']."\r\nReply-to:".$_REQUEST['email']);
	}
}
?>