<?

class field {


	function is_valid_email($email){
		
		$isValid = true;
		$atIndex = strrpos($email, "@");
		if (is_bool($atIndex) && !$atIndex){
			$isValid = false;
		}
		else{
		   
			$domain = substr($email, $atIndex+1);
			$local = substr($email, 0, $atIndex);
			$localLen = strlen($local);
			$domainLen = strlen($domain);
			
			if ($localLen < 1 || $localLen > 64){
			
				// local part length exceeded
				$isValid = false;
			}
			else if ($domainLen < 1 || $domainLen > 255){
			
				// domain part length exceeded
				$isValid = false;
			}
			else if ($local[0] == '.' || $local[$localLen-1] == '.'){
			
				// local part starts or ends with '.'
				$isValid = false;
			}
			else if (preg_match('/\\.\\./', $local)){
			
				// local part has two consecutive dots
				$isValid = false;
			}
			else if (!preg_match('/[\\.]+/', $domain)){
			
				// domain part has at least one dots
				$isValid = false;
			}
			else if ($domain[0] == '.' || $domain[$domainLen-1] == '.'){
			
				// domain part starts or ends with '.'
				$isValid = false;
			}
			else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)){
			
				// character not valid in domain part
				$isValid = false;
			}
			else if (preg_match('/\\.\\./', $domain)){
			
				// domain part has two consecutive dots
				$isValid = false;
			}
			else if(!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local))){
			
				// character not valid in local part unless 
				// local part is quoted
				if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\","",$local))){
					$isValid = false;
				}
			}
		}
		
		return $isValid;
	}
	
	
	function is_valid_date($date, $format="dd/mm/yyyy"){
		
		if($format == "dd/mm/yyyy"){
			if(preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/i", $date)){return true;}
		}
		
		return false;
	}
	
	
	function is_date_in_future($date, $format="dd/mm/yyyy"){
		
		if($format == "dd/mm/yyyy"){
			
			$today_ymd = date("Ymd");
			$date_parts = explode("/", $date);
			$date_ymd = $date_parts[2].$date_parts[1].$date_parts[0];
			
			if($date_ymd > $today_ymd){
				return true;
			}
		}
		
		return false;
	}
	
	
	function is_number($string){
	
		if(preg_match("/^-{0,1}[0-9]+$/", $string) == 1){
			return true;
		}
		return false;
	}
	
}


?>