<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="RMG Lettings" />
<meta name="description" content="Affordable Student Accommodation in Manchester" />
<meta name="keywords"  content="student,homes,manchester,lettings,monty hall,montgomery house,enquire now,student accommodation,affordable student accomodation" />
<meta name="author" content="Tejaswy">
<meta name="Resource-type" content="Document" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>RMG Lettings</title>


<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="css/conSlider.css" media="screen" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" charset="utf-8" href="css/gothambold.css" />
<link rel="stylesheet" type="text/css" charset="utf-8" href="css/gotham.css" />
<link rel="stylesheet" href="css/font-awesome.min.css">
<!--[if lte IE 8]>
<link href="css/lte_ie8.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->
<!-- <link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css" />-->
<link href="css/themes_smoothness_jquery-ui.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="css/responsive-tables.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="css/common.css" rel="stylesheet" type="text/css" media="screen" />


<!--[if IE]>
<script type="text/javascript">
var console = { log: function() {} };
</script>
<![endif]-->

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<!-- <script type="text/javascript" src="js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="js/jquery.fullPage.min.js"></script> -->
<script type="text/javascript" src="js/jquery.conSlider.js"></script> 
<script type="text/javascript" src="js/respond.min.js"></script> 
<script type="text/javascript">
	$(document).ready(function() {
		
		 $(".date").datepicker({dateFormat : "dd/mm/yy", changeYear : true, minDate : "+0D"});

			//conslider
		  $('#lettings_slider').conSlider({
			 bootstrap_ui : true
			});
			$('#student_slider').conSlider({
				bootstrap_ui : true
			});
			$('#gallery_slider').conSlider({
				'theme': 'gallery',
				'menuPosition': 'bottom',
				bootstrap_ui : true
			}); 

			fnMenuItemActive();
				
			$('.headerlinks').click(function(){
				$('.headerlinks').removeClass('active');
			   $(this).addClass('active');
			   //close menu div after click
			  
			  /*  if( /mobile/i.test(navigator.userAgent) ) { 
				   $('.nav').hide();
			   } */
			   
			   			   
			});

			$('.faqs dd').hide(); // Hide all DDs inside .faqs
			$('.faqs dt').hover(function(){$(this).addClass('hover')},function(){$(this).removeClass('hover')}).click(function(){ // Add class "hover" on dt when hover
			$(this).next().slideToggle('normal'); // Toggle dd when the respective dt is clicked
			}); 


			// Changing menu dependent on section
			$(window).scroll(function() {
			    
			    var windowTop = Math.max($('body').scrollTop(), $('html').scrollTop());
			    
			    $('.jsection').each(function (index) {

				    var sectionID = $(this).attr('id');

				   /*  alert('windowtop:'+windowTop);
				    alert('position:'+$(this).position().top); */
				    
			        if (windowTop > ($(this).position().top - 500))
			        {
			            $('#mymenu li.active').removeClass('active');
			            $('#mymenu a').each(function(){
									if($(this).attr('href').replace('/#', '') == sectionID){
										$(this).parent().addClass('active');
									}
				            });
			        }
			    });
			    
			}).scroll();
					
	});
	
	function fnMenuItemActive() {
		//activate menu item
		var url = window.location.href;
		var pieces = url.split("#");
		
		if(typeof pieces[1] !== 'undefined') {
			
			$('#'+pieces[1]+'-links').addClass('active');
		}
		else
		{
			var url_param = url.split("/");
			if(typeof url_param[1] == 'undefined' || url_param[1].length == 0)
				$('#Home-links').addClass('active');
		}
	}


//make an enquire page form submission
function send_form(){
document.form1.submit();
}

		/****Responsive  Sticky Header ****/
$(function() {var $document = $(document),
$element = $('#headerContainer'),
className = 'stickyNav';
$document.scroll(function() {if ($document.scrollTop() >= 300) {
$element.addClass("stickyNav" );}
else {$element.removeClass("stickyNav");}});});



</script>
<? require_once("utils.php");?>
<? require_once("includes/analytics.php");?>
</head>
<body>
<?php include_once 'includes/header.php';?>
<!--  <div id="fullpage">-->
	<div class="jsection"  id="Home">
		<div class="container" id="headerbg">
		<!--[if lte IE 8]><div id="bg"><img src="/images/header-img.png" alt=""></div><![endif]-->
		<div class=" container_padding">
				<div class="row">
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 paddtop15 enquireform">
					  	<img class="img-responsive" src="/images/logo.png"/>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 paddtop15 enquireform">
						<img class="img-responsive" src="../images/Badge90.png"/>
					</div>
				</div>
				<div class="row">
				  <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 wel-txt-div paddtop15 enquireform paddleft30">
				  		<p class="yellowfont fontbg">Welcome to<br/></p>
				  		<p class="whitefontgotham fontbg">RMG Lettings<br/></p>
				  		<p class="whitefont fontmd">RMG Lettings can assist with your Manchester student accommodation needs.  <br/>
							Our Manchester Student Halls - Monty Halls - are suitable for local, international, ERASMUS exchange students, or language school students who are looking for a minimum of a 37 week let.
						</p>
				  		<p class="whitefont fontmd">
				  		To book a room or for any further enquiries please fill in the <a href="#Contact_us" class="whitefont fontlg">enquiry form</a>.<br/> 
							Alternatively you can contact us on <span class="fontlg"><?php echo $UTILS_TEL_LETTINGS_MAIN;?></span> or email us at <?=$UTILS_CONTACT_EMAIL_TAG_WHITE?>
						</p><br/>
			  		<div class="enquirenow-btn"><a href="#Contact_us"><div class=" pull-left btn-custom btn-lg fontmd">Enquire Now</div></a></div>
			  		
				  	</div>
				  	<div class="contactusbox-padd enquireform">
						<div class="contactusbox dblueborder">
							<div class="contactusbox-row">
								<div class="contactusbox-icon-phone bluebg topleftradius text-center" >
									<i class="fa fa-phone yellowfont"></i>
								</div>
								<div class="contactusbox-txt yelbg toprightradius text-center contactusbox-txt-phone"  >
									<span class="bluefontgotham"><?php echo $UTILS_TEL_LETTINGS_MAIN;?></span>
								</div>
							</div>
							<div class="contactusbox-row">
								<div class="contactusbox-icon-email bluebg bottomleftradius text-center" >
									<i class="fa fa-envelope yellowfont"></i>
								</div>
								<div class="contactusbox-txt bottomrightradius yelbg text-center contactusbox-txt-email" >
									<a href='ma&#105;lto&#58;i%6Efo&#64;rmgletti&#110;gs%2&#69;co%&#50;E&#117;k' style='text-decoration:none'  class='bluefontgotham '><bold>info@rmglettings.co.uk</bold></a>
								</div>
							</div>			
						</div>
					</div>
				</div>
				<div class="row">
					<center><a href="#Info" title="Usefull Info"><i class="fa fa-arrow-circle-down fonticon yellowfont"></i></a></center>
				</div>
			</div>
		
			
		</div>
	</div>
	
	<div class="section jsection" id="Info">
	<div class="body-conslider">
	   <div id="lettings_slider" class="conSlider blueborder">
	   
			<div class="con-menu">
				<ul>
					<li><a href="#facilities"><span class="bluefontgotham fontlg">Facilities</span></a></li>
					<li><a href="#rates"><span class="bluefontgotham fontlg">Rates</span></a></li>
					<li><a href="#details"><span class="bluefontgotham fontlg">Details</span></a></li>
					<li><a href="#amenities"><span class="bluefontgotham fontlg">Amenities</span></a></li>
					<li><a href="#location"><span class="bluefontgotham fontlg">Location</span></a></li>
					<li><a href="#what_to_bring"><span class="bluefontgotham fontlg">What to bring</span></a></li>
				</ul>
			</div>
			
			<div  class="con-container">
				<div class="con-slides"> 
					
					<div class="con-slide" data-slide-id="facilities">
						<h3 class="bluefontgotham fontxl">Facilities</h3>
		                    <p class="greytxt fontmd">In your room you will have a Double bed, Wardrobe, Fitted Carpets, Curtains/Blinds, TV Point and Sink.</p>
		                    <p  class="fontmd">All sites offer the following:                      
		                  </p>
		                  <ul class="squareul fontmd">
		                            <li>Night Security</li>
		                            <li>Secure Fob Entry</li>
		                            <li>CCTV</li>
		                            <li>Dedicated management team on site Monday to Friday between 09:00 and 17:00</li>
							  		<li>Customer Service contactable 24 hours a day, 365 days a year</li>
		                            <li>Central Locations</li>
		                            <li>Free Parking</li>
									<li>Direct line to taxi service</li>
		                            <li>Fitted Kitchen: Hob Oven, Fridge, Freezer, Toaster, Kettle, Microwave</li>
		                            <li>On-site Laundry Facilities</li>
		                            <li>Communal Relaxation Areas</li>
		                            <li>TV point in every room</li>
		                            <li>2 acre communal garden</li>
									<li>Vending machines</li>
		                            
		                        </ul>
					</div>
					<div class="con-slide" data-slide-id="rates">
						<h3 class="bluefontgotham fontxl">Rates</h3>
							<p class="greytxt fontmd">The rent includes all bills and internet access. Please find below breakdown of the tariff:</p>
							<!--<p class="fontmd">
								At RMG Lettings we pride ourselves on flexibility and have a minimum stay of 10 weeks.
							</p>-->
							<table class="table">
								<thead>
								<tr class="bg-primary">
									<th>Tenancy Length</th>
									<th>Start</th>
									<th>Finish</th>
									<th class="text-right">Rates</th>
								</tr>
								</thead>
								<tbody>
								<!--<tr class="btn-danger">
									<td data-th="Tenancy Length">10 weeks</td>
									<td data-th="Start">12th September</td>
									<td data-th="Finish">20th November</td>
									<td data-th="Rates" class="text-right">All Lets Agreed</td>
								</tr>
								<tr class="btn-danger">
									<td data-th="Tenancy Length">20 weeks</td>
									<td data-th="Start">12th September</td>
									<td data-th="Finish">29th January</td>
									<td data-th="Rates" class="text-right">All Lets Agreed</td>
								</tr>-->
								<tr class="active">
									<td data-th="Tenancy Length">37 weeks</td>
									<td data-th="Start">10<sup>th</sup> September 2016</td>
									<td data-th="Finish">20<sup>th</sup> May 2017</td>
									<td data-th="Rates" class="text-right"><span class="label label-pill label-primary">Early Bird Rate</span> £90.00</td>
								</tr>
								<tr class="warning">
									<td data-th="Tenancy Length">44 weeks</td>
									<td data-th="Start">10<sup>th</sup> September 2016</td>
									<td data-th="Finish">8<sup>th</sup> July 2017</td>
									<td data-th="Rates" class="text-right"><span class="label label-pill label-primary">Early Bird Rate</span> £90.00</td>
								</tr>
								<tr class="active">
									<td data-th="Tenancy Length">44 weeks+</td>
									<td data-th="Start">10<sup>th</sup> September 2016</td>
									<td data-th="Finish">Subject to Agreement</td>
									<td data-th="Rates" class="text-right"><span class="label label-pill label-primary">Early Bird Rate</span > £90.00</td>
								</tr>
								</tbody>
							</table>
					</div>
					
					<div class="con-slide" data-slide-id="details">
						<h3 class="bluefontgotham fontxl">Details</h3>
						<div class="padleft15 fontmd">
							<span class="lbluefont fontlg">Main House</span>
							<p>
								There are 4 floors in this building which are all provided with
								a communal kitchen. The floors are made up of separate wings for
								male and female residents, each containing 20 rooms. Also located
								on each floor is laundry access, and a communal bathroom that
								contains 2 showers, 1 bath and 4 toilets, there is a disabled
								toilet located on the ground floor. All bedrooms come with a
								double bed, wardrobe, desk, sink and gas central heating.
							</p>
							<p>
								The rooms on the ground floor are particularly sought after due
								to their location and view overlooking the 2 acre garden. The
								garden comes complete with football nets, volleyball nets, outdoor
								seating area and BBQ equipment.
							</p>
							<!--  <p>
								There is a small gym room with rower, running machine, power
								plate, cross trainer and bike and 2 sets of free weights and
								benches.							</p> -->
							<span class="lbluefont fontlg">Annex<br/></span>
							<p>
								The Annex is located within the grounds area and there are 4
								floors which each benefit from large kitchen and lounge areas.
								There are 20 rooms per floor and 2 large bathrooms per floor with
								2 showers, 1 bath and 2 toilets. The floors have single sex
								option. This building is more appealing to a student who prefers a
								quieter lifestyle.
							</p>
							<p>
								"We have the option of both single sex and mixed sex floors in each building." 
							</p>
						</div>
					</div>	
					
					<div class="con-slide fontmd" data-slide-id="amenities">
						<h3 class="bluefontgotham fontxl">Amenities</h3>
							<ul class="squareul">
								<li>Tesco's - 0.8miles</li>
								<li>Shell Garage - 500ft</li>
								<li>Subway - 0.7 miles</li>
								<li>Asda - 1.5 miles</li>
							</ul>
							<span class="lbluefont fontlg padleft15">Campuses</span>
							<ul class="squareul">
								<li>Manchester Metropolitan Hollings Campus - 2 miles</li>
								<li>Manchester Metropolitan All Saints Campus - 1.9 miles</li>
								<li>University of Manchester Fallowfield Campus - 1.8 miles</li>
								<li>University of Manchester Oxford Road Campus - 2 miles</li>
								<li>Excel College - 2.6 miles </li>
								<li>Berlitz College - 2.5 miles</li>
								<li>The International Society - 1.8 miles</li>
								<li>Manchester Academy Of English - 1.3 miles</li>
								<li>EF International Language Schools - 1.4 miles</li>
								<li>Monty Hall - 1.5 miles</li>
								<li>Italian Language Solutions - 0.6 miles</li>
							</ul>
					</div>
					
					<div class="con-slide" data-slide-id="location">
						<h3 class="bluefontgotham fontxl">Location</h3>
							<div class="fontmd">
								<div class="greytxt">Click on the map to see where this accommodation is located.<br/><br/></div>
									<div class="container">
										<div class="row">
											<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
												<a href="https://maps.google.co.uk/maps?q=Demesne+Road,+Manchester+M16+8PH&hl=en&oe=utf-8&client=firefox-a&hnear=Demesne+Rd,+Manchester+M16,+United+Kingdom&t=m&z=16&iwloc=A"
													target="_blank">
													<img class="img-responsive map" src="../images/map.jpg" />
												</a>
											</div>
											<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
												<br/>
												Demesne Road,<br /> 
												Manchester<br />
												M16 8PH
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<br/>
											"The bus that travels between Alexandra Park and Piccadilly station is the number 85 Stagecoach bus and passes all universities." Or "the 101 From princess park way" -

								 	For bus time tables please follow the link below <a href="http://www.tfgm.com/buses/Pages/default.aspx" target="_blank">http://www.tfgm.com/buses/ </a>

											</div>
										</div>
									</div>
							</div>
					</div>
					
					<div class="con-slide" data-slide-id="what_to_bring">
						<h3 class="bluefontgotham fontxl">What to Bring</h3>
						<div class="fontmd">
							<p>As you are living away from home, you will need to bring the following items with you:</p>
			
							<ul class="squareul">
								<li>Clothes</li>
								<li>Bedding (including pillows)</li>
								<li>Towels</li>
								<li>Cutlery and crockery</li>
								<li>Pots, pans and cooking utensils</li>
								<li>Tea towels</li>
								<li>Coat hangers</li>
								<li>Personal computer</li>
								<li>Lamp suitable for task lighting</li>
								<li>Radio/TV (if required)</li>
								<li>Iron and Ironing Board</li>
								<li>Stationery</li>
							</ul>
							<p>
								We can provide a bedding pack for international students who are unable to bring this with them. Please ensure this is requested before you arrive.<br/>
								
								Not all rooms are equipped with a desk chair. If you wish to have one in your room, please check if you need to bring one with you.<br/>
								
								If you are bringing a TV with you, you will need to apply for a TV licence which you can do online. (<a href="http://www.tvlicensing.co.uk" target="_blank">http://www.tvlicensing.co.uk</a>)<br/>
							</p>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	</div>
	
	<!--  Student Life Starts -->
	<div class="section jsection" id="Student_life">
		<div class="body-conslider">
		   <div id="student_slider" class="conSlider blueborder">
				<div class="con-menu">
					<ul>
						<li><a href="#montyhall"><span class="bluefontgotham fontlg">Accommodation</span></a></li>
						<li><a href="#arndale"><span class="bluefontgotham fontlg">Shopping & Nightlife</span></a></li>
						<li><a href="#operahouse"><span class="bluefontgotham fontlg">Culture</span></a></li>
					</ul>
				</div>
				<div class="con-container fontmd">
					<div class="con-slides"> 						
						<div class="con-slide" data-slide-id="montyhall">
							<h3 class="bluefontgotham fontxl">Accommodation</h3>							
							<p>If you are looking for student accommodation in Manchester which is great value affordable housing with all inclusive bills and 
							internet costs then a room at Monty Halls could be your new home. Whether you are at University, College or studying in the city centre we are only a short bus 
							ride away or walk located in the idyllic surroundings of Whalley Range.</p>
							<p>There is a large cascade of fun and interesting things to do in and around Montgomery Halls; there is a range of different bars, restaurants, museums, clubs pubs,
							 shops and much more. Whether you're into music and socialising or more energetic pursuits like biking and rock climbing there is something for you which is 
							 probably what make it the largest student community in the UK and Europe.</p>
							 <div class="accommodation">
								<span class="lbluefontgotham fontxl overlay">Montgomery House</span>									
							</div>
						</div>
						
						<div class="con-slide" data-slide-id="arndale">
							<h3 class="bluefontgotham fontxl">Shopping & Nightlife</h3>							
							<p>Manchester is a great shopping experience, including the Arndale Centre, The Trafford Centre, Affleck's and Market Street shopping areas plus Selfridges, 
								Harvey Nicholls and House of Fraser.  As well as several smaller shops in areas such as Chorlton and Withington within a short distance of our student accommodation. 
								For restaurants, bars and Cinemas why not head to the Printworks where there is a really vibrant nightlife. 
								Ranging from a night at the cinema to drinks with friends. Other areas that are a must visit for a night out include Canal Street, 
								The Northern Quarter, Deansgate and Deansgate Locks and many student bars on and around Oxford road. If it's music you're into then there are many bars and clubs, 
								including many which off live music ranging from smaller venues such as the Academy and O2 Apollo to  MEN Arena, the palace theatre  and the opera house for live 
								shows visit the Box office. If its food your after you could also try Manchester's China Town or Didsbury town centre which is just a short bus trip away.</p>
							<div class="arndale">
								<span class="lbluefontgotham fontxl overlay2">Manchester Arndale</span>
							</div>
								
						</div>
						<div class="con-slide" data-slide-id="operahouse">
							<h3 class="bluefontgotham fontxl">Culture</h3>							
							<p>If you are looking for some culture there are several world class museums, art galleries and several world famous sports venues including Premier League
							 giants Manchester United and Manchester City football clubs. You can also visit Old Trafford Cricket Ground, one of the UK's major international cricket venues or 
							 there's the National Football Museum, Science and Industry, The Imperial War Museum North and The Manchester Museum, plus both the Whitworth Art Gallery and the 
							 Manchester Art Gallery.</p>
							<p>How about something to keep active why not try one of our many attractions such as Manchester aquatics centre, The dance house, Sale water parks, Manchester climbing 
							centre, Airkix where you can really fly; the Chill factor if ski, snowboard, or air boarding take your fancy. Or maybe you would prefer a slower pace try one of our 
							many spa hotel or maybe something in between they one of our indoor climbing centres or a Vertical chill ice wall.</p>						
							<p>What ever it is you're looking for you can find in here making Manchester the perfect city to get the most out of student life.</p>
							<div class="opera">
								<span class="lbluefontgotham fontxl overlay3">Opera House</span>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
	<!--  Student Life ends -->	
		<!-- Gallery Start -->
	<?php 
		$pics[1494] = 'House rear';
		$pics[1466] = 'House Bedroom';
		$pics[1467] = 'House Common Room1';
		$pics[1474] = 'House Common Room2';
		$pics[1488] = 'House BBQ area';
		$pics[6775] = 'Exterior Rear';
		$pics[1447] = 'Annex Bathrooms';
		$pics[1450] = 'Annex Lounge';
		$pics[1453] = 'Annex Kitchen';
		$pics[1459] = 'Annex Bedroom';
		$pics[1532] = 'Alexandra Park';
		$pics[1501] = 'Opera House';
		$pics[1502] = 'Manchester Arndale';
	?>
	<div class="section jsection" id="Gallery">
		<div class="gallery-conslider">		
			<div id="gallery_slider" class="conSlider">
				<div class="con-menu">
					<ul>
					<?php  foreach($pics as $key =>$val) {?>
						<li><a href="#<?= $val;?>"><img src="images/Mont_<?=$key;?>.jpg" alt="<?= $val;?>" title="<?= $val;?>" class="img-responsive" /><span class="bluefontgotham fontxl"><?= $val;?></span></a></li>
					<?php  }?>
					</ul>
				</div>
				<div class="con-container fontmd">
					<div class="con-slides"> 		
						<?php  foreach($pics as $img_id =>$title) {?>				
						<div class="con-slide" data-slide-id="<?= $title;?>">
							<img src="images/Mont_<?=$img_id;?>.jpg" alt="<?= $val;?>" title="<?= $val;?>" class="img-responsive img-center" />
						</div>	
						<?php  }?>					
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Gallery End -->

	<!-- video Section -->
	<!--<div class="section jsection" id="Video">
		<div class="video-container">
		    <iframe src="http://www.youtube.com/embed/dPmIXJsMZlw" allowfullscreen="" frameborder="0">
		    </iframe>
		</div>
	</div>-->

	
	<!--  Make an enquire Page Start -->
	<div id="Contact_us" class="jsection">
		<div class="intro container_padding">
			<?php require_once 'contact.php';?>	
			<div class="whitebg blueborder" id="contact">	
				<div class="container paddlrb15">
					<div class="bluefontgotham fontbg">Make an Enquiry</div>
					<? if($_REQUEST['a'] == "s" && $save_result !== true){?>
	                <p class="text-danger fontmd"><?=$save_result?></p>
	                <? }?>
	                
	                <? if($_REQUEST['a'] == "s" && $save_result === true){?>
		                <p class="text-success fontmd">Thank you for your enquiry. Someone will be in touch with you soon.</p>
	                <? }else{ ?>
					<p class="text-primary fontmd">To book a room or for any further enquiries please fill complete the enquiry form below. (<span class="text-danger">*</span>) required fields.
					</p>
					<div class="row">
					<form role="form" id="form1" name="form1" method="post">
	                    <input type="hidden" name="a" value="s" />
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 fontmd enquireform">
							  <div class="form-group">
							    <label for="title">Title</label>
							    <select class="form-control" name="title" id="title">
	    	                        	<option value="" selected="selected">-- please select --</option>
	                                    <option value="Ms" <? if($_REQUEST['title'] == "Ms"){?>selected="selected"<? }?>>Ms</option>
	                                    <option value="Miss" <? if($_REQUEST['title'] == "Miss"){?>selected="selected"<? }?>>Miss</option>
	                                    <option value="Mrs" <? if($_REQUEST['title'] == "Mrs"){?>selected="selected"<? }?>>Mrs</option>
	                                    <option value="Mr" <? if($_REQUEST['title'] == "Mr"){?>selected="selected"<? }?>>Mr</option>
								</select>
							  </div>
							<div class="form-group">
							    <label for="first_name">First Name(s)&nbsp;<span class="text-danger">*</span></label>
							    <input type="text"  class="form-control" name="first_name" id="first_name" value="<?=$_REQUEST['first_name']?>"/>
							  </div>
							  <div class="form-group">
							    <label for="last_name">Surname&nbsp;<span class="text-danger">*</span></label>
							    <input type="text"  class="form-control" name="last_name" id="last_name" value="<?=$_REQUEST['last_name']?>"/>
							  </div>
							  <div class="form-group">
							    <label for="email">Email&nbsp;<span class="text-danger">*</span></label>
							    <input type="email"  class="form-control" name="email" id="email" value="<?=$_REQUEST['email']?>"/>
							  </div>
							  <div class="form-group">
							    <label for="tel">Telephone no.&nbsp;<span class="text-danger">*</span></label>
							    <input type="text"  class="form-control" name="tel" id="tel" value="<?=$_REQUEST['tel']?>" />
							  </div>
							 <!-- <div class="form-group">
							    <label for="development">Which development are you enquiring about?&nbsp;<span class="text-danger">*</span></label>
							     <select class="form-control" name="development" id="development">
	    	                        	<option value="" selected="selected">-- please select --</option>
	                                    <option value="Manchester" <? //if($_REQUEST['development'] == "Manchester"){?>selected="selected"<? //}?>>Manchester</option>
	                                    <option value="None" <? //if($_REQUEST['development'] == "None"){?>selected="selected"<? //}?>>None</option>
								</select>
							  </div> --> 
							  <div class="form-group">
							    <label for="address">Current address&nbsp;<span class="text-danger">*</span></label>
							     <textarea class="form-control" name="address" id="address" rows="3"></textarea>
							  </div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 fontmd enquireform">
							<div class="form-group">
							    <label for="move_in_date">Move-in date&nbsp;<span class="text-danger">*</span></label>
							    <input type="text"  class="form-control date" name="move_in_date" id="move_in_date" value="<?=$_REQUEST['move_in_date']?>"/>
							  </div><div class="form-group">
							    <label for="move_out_date">Move-out date&nbsp;<span class="text-danger">*</span></label>
							    <input type="text"  class="form-control date" name="move_out_date" id="move_out_date" value="<?=$_REQUEST['first_name']?>" />
							  </div>
							  <div class="form-group">
							    <label for="first_name">Preferred method of contact&nbsp;<span class="text-danger">*</span></label>
							   	<select class="form-control" name="method" id="method">
	    	                        	<option value="" selected="selected">-- please select --</option>
	                                    <option value="Telephone" <? if($_REQUEST['method'] == "Telephone"){?>selected="selected"<? }?>>Telephone</option>
	                                    <option value="Email" <? if($_REQUEST['method'] == "Email"){?>selected="selected"<? }?>>Email</option>
	                            </select>
							  </div>
							  <div class="form-group">
							    <label for="comments">Comments / questions</label>
							    <textarea class="form-control" name="comments" id="comments" rows="7"><?=$_REQUEST['comments']?></textarea>
							  </div>
							  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-superlg-6 btn-custom btn-lg fontmd" style="width:100%" onclick="send_form();return false;" title="Click to send">Enquire Now</div>
						</div>
						<div class="scrollup"><a class="scrolltohome"></a></div>
					</div>	
					<?php }?>	
				</div>
				</div>
		</div>
	</div>
	<div class="jsection" id="Faqs">
		<div class="container_padding">
		   <div class="whitebg blueborder paddleft25 fontmd faqs">
   			<p class="bluefontgotham fontxl">Parking</p>
		   <dl>
		        <dt>Can I bring my car?</dt>
		        <dd>Of course, we have free on site car parking for any residents. </dd>
		        <dt>What if my friend comes to visit?</dt>
		        <dd>They can park for free as long as it doesn't affect the other tenants; otherwise there is on street parking just outside the main entrance. </dd>
				<dt>Are there bike storage facilities on site?</dt>
			    <dd>Yes, we have plenty of secure rack space on site</dd>
		    </dl>
		    <br/>
		    <p class="bluefontgotham fontxl">Cleaning</p>
		    <dl>				    
		        <dt>Whose responsibility is it to clean my room?</dt>
		        <dd>It's yours own responsibility to clean your own room.  </dd>
		        <dt>Are there cleaners for the communal areas?</dt>
		        <dd>Yes - Our onsite cleaning team will clean the communal areas 6 days a week and attend on Sunday to empty the bins. </dd>
			</dl>
		    <br/>
			<p class="bluefontgotham fontxl">Communal areas</p>	
			<dl>			    
		        <dt>Is there a common room?</dt>
		        <dd>Yes. We have fantastic on site common room with flat screen TV, pool table, sofas, vending machine, games machine and bar area.</dd>
		        <dt>Is there a TV in the common room?</dt>
				<dd>There are flat screen TVs available in the common room.</dd>						
				<dt>How is it decided what we watch on the TV?</dt>
				<dd>That's completely up to the users of the common room - but we do recommend a first come first served policy.</dd>	
			</dl>
		    <br/>
			<p class="bluefontgotham fontxl">Room queries</p>
			<dl>
		        <dt>Do I need to bring any furniture/is there room for my own furniture?</dt>
		        <dd>All of our rooms are supplied with beds, wardrobes and desks ; but there is some room for additional furniture within reason. We also have a handle full of larger rooms on the ground floor of the main building which have an additional 5 square meters but these tend to go quick.</dd>
		        <dt>Can I change my room?</dt>
		        <dd>If you would like to change your room after moving in, then depending on the reason we can do this subject to availability. </dd>
		        <dt>Can I have guests in my room?</dt>
		        <dd>They are allowed for a couple of days, no overnight visitors longer than two consecutive nights.</dd>
			</dl>
	        <br/>
	        <p class="bluefontgotham fontxl">Bills and Deposits.</p>
	        <dl>
		         <dt>Do I have to pay for the internet, electricity, gas or water?</dt>
	        	 <dd>No, all utilities and internet are included in your rent.</dd>
		        <dt>How much deposit is requred?</dt>
		        <dd>We take a &pound;150 deposit which will be held as a damage deposit on commencement of your tenancy. </dd>	
		    </dl>			        
	       	<br/>
       	    <p class="bluefontgotham fontxl">Laundry</p>
       	    <dl>
		        <dt>Where do I wash my clothes?</dt>
		        <dd>Each floor has its own laundry room with washer and dryer which are operated using a top up card that can be purchased from a machine in the main lobby and topped up online using the circuit website.</dd>
		        <dt>Is detergent provided?</dt>
		        <dd>No you will need to supply your own detergent & conditioner.</dd>	
		        <dt>Is the laundry available 24 hours a day?</dt>
		        <dd>Yes</dd>
		    </dl>			   
		   	<br/>
		   	<p class="bluefontgotham fontxl">Repairs and maintenance</p>
		   	<dl>
		        <dt>What happens if there is a breakage or problem with my room?</dt>
		        <dd>We ask that any maintenance issue is reported to us in the main office or via email within 48 hrs of it first being noticed.</dd>
		    </dl>			      
			<br/>					  
		    <p class="bluefontgotham fontxl">Eating</p>	
		    <dl>			    
		        <dt>Do I have to supply my own food and do my own cooking?</dt>
		        <dd>The short answer is yes. We are close to several local supermarkets (Asda,Tesco express) where you can purchase any essentials.</dd>
		    </dl>
		    <br/>
		    <p class="bluefontgotham fontxl">Other queries</p>	
		    <dl>			    
		        <dt>Heating & Hot Water</dt>
		        <dd>The heating and hot water are on a timer and in addition the heating can be controlled by a valve on the side of the radiator in each room.</dd>
		         <dt>Are pets allowed?</dt>
		        <dd>NO, we are a strictly no pets property.</dd>
		        <dt>Any special facilities for students with disabilities? </dt>
		        <dd>We have  wheel chair ramps at the front of the main building and a disabled bathroom on the ground floor.</dd>
			    <dt>Security</dt>
		        <dd>We have CCTV and night security on site from 10pm till 6am in addition to the onsite staff who will be in the office and around the building until 5:30pm.</dd>
		        <dt>Are there any recycling arrangements?</dt>
		        <dd>You can find recycling bin in each of the kitchens and can contact the building management or local authority for removal of larger items. In addition to this we have a clothing bin for donations to the British heart foundation.</dd>
		        <dt>Where can I collect my mails? </dt>
		        <dd>All posts can be collected from the office by the main door if you receive a large parcel our friendly night staff will post a slip under your door. Or alternatively some parcels maybe taken to the nearest post office (Post office collection locations - <a href="http://www.postoffice.co.uk/branch-finder" target="_blank">http://www.postoffice.co.uk/branch-finder</a> )</dd>
	        </dl>
   		
		   </div>
		 </div>
			<center><a href="#Home"><i class="fa fa-arrow-circle-up bluefont fonticon"></i></a></center><br/>
	</div>
	<!--  Make an enquire Page ends -->	
	<?php include_once 'includes/footer.php';?>
	
	<!-- </div> -->
</body>
</html>