<?php
require_once($UTILS_SERVER_PATH."library/core.class.php");
require_once($UTILS_SERVER_PATH."library/country.class.php");
require_once($UTILS_SERVER_PATH."library/field.class.php");


class applications extends core {
	
	var $lettings_app_id;
	var $lettings_app_serial;
	var $lettings_app_input_type_id;
	var $lettings_app_home_first_name;
	var $lettings_app_home_middle_name;
	var $lettings_app_home_last_name;
	var $lettings_app_home_add_1;
	var $lettings_app_home_add_2;
	var $lettings_app_home_add_3;
	var $lettings_app_home_add_town;
	var $lettings_app_home_add_county;
	var $lettings_app_home_add_postcode;
	var $lettings_app_home_add_country_id;
	var $lettings_app_home_tel_1;
	var $lettings_app_home_tel_2;
	var $lettings_app_email;
	var $lettings_app_dob_ymd;
	var $lettings_app_gender;
	var $lettings_app_nationality;
	var $lettings_app_uni_name;
	var $lettings_app_uni_course;
	var $lettings_app_uni_student_id;
	var $lettings_app_stay_start_ymd;
	var $lettings_app_stay_end_ymd;
	var $lettings_app_kin_required;
	var $lettings_app_kin_first_name;
	var $lettings_app_kin_last_name;
	var $lettings_app_kin_same_address;
	var $lettings_app_kin_add_1;
	var $lettings_app_kin_add_2;
	var $lettings_app_kin_add_3;
	var $lettings_app_kin_add_town;
	var $lettings_app_kin_add_county;
	var $lettings_app_kin_add_postcode;
	var $lettings_app_kin_add_country_id;
	var $lettings_app_kin_tel_1;
	var $lettings_app_kin_tel_2;
	var $lettings_app_guarantor_required;
	var $lettings_app_guarantor_first_name;
	var $lettings_app_guarantor_last_name;
	var $lettings_app_guarantor_relationship;
	var $lettings_app_guarantor_email;
	var $lettings_app_guarantor_same_address;
	var $lettings_app_guarantor_add_1;
	var $lettings_app_guarantor_add_2;
	var $lettings_app_guarantor_add_3;
	var $lettings_app_guarantor_add_town;
	var $lettings_app_guarantor_add_county;
	var $lettings_app_guarantor_add_postcode;
	var $lettings_app_guarantor_add_country_id;
	var $lettings_app_guarantor_tel_1;
	var $lettings_app_guarantor_tel_2;
	var $lettings_app_notes;
	var $lettings_app_created_ymdhis;
	var $lettings_app_transferred_to_intranet_ymdhis;
	var $lettings_app_discon;
	var $lettings_app_discon_ymdhis;
	
	var $error_fields;
	
	
	function __construct($ref="", $ref_type="id", $audit_table=false){
		
		$this->error_fields = array();
		$this->error_fields['field_names'] = array();
		$this->error_fields['save_msg'] = "";
		
		$this->populate($ref, $ref_type, $audit_table);
	}
	
	
	function populate($ref="", $ref_type="id", $audit_table=false){
		
		$mysql = new mysql;
		$security = new security;
		
		if( $ref != "" ){
			
			if( $ref_type == "serial" ){
				$ref_clause = " lettings_app_serial = '".$security->clean_query($ref)."' ";
			}
			elseif( $ref_type == "online_code" ){
				$ref_clause = " lettings_app_online_code = '".$security->clean_query($ref)."' ";
			}
			else{
				$ref_clause = " lettings_app_id = ".$security->clean_query($ref)." ";
			}	
			
			if( $audit_table === true ){
				$table_clause = "lettings_apps_audit";
			}
			else{
				$table_clause = "lettings_apps";
			}
			
			$result = $mysql->query("
			SELECT * 
			FROM 
			".$table_clause." 
			WHERE 
			".$ref_clause);
			$row = $mysql->fetch_array($result);
		}
			
		$this->lettings_app_id = $row['lettings_app_id'];
		$this->lettings_app_serial = $row['lettings_app_serial'];
		$this->lettings_app_online_code = $row['lettings_app_online_code'];
		$this->lettings_app_input_type_id = $row['lettings_app_input_type_id'];
		$this->lettings_app_home_first_name = $row['lettings_app_home_first_name'];
		$this->lettings_app_home_middle_name = $row['lettings_app_home_middle_name'];
		$this->lettings_app_home_last_name = $row['lettings_app_home_last_name'];
		$this->lettings_app_home_add_1 = $row['lettings_app_home_add_1'];
		$this->lettings_app_home_add_2 = $row['lettings_app_home_add_2'];
		$this->lettings_app_home_add_3 = $row['lettings_app_home_add_3'];
		$this->lettings_app_home_add_town = $row['lettings_app_home_add_town'];
		$this->lettings_app_home_add_county = $row['lettings_app_home_add_county'];
		$this->lettings_app_home_add_postcode = $row['lettings_app_home_add_postcode'];
		$this->lettings_app_home_add_country_id = $row['lettings_app_home_add_country_id'];
		$this->lettings_app_home_tel_1 = $row['lettings_app_home_tel_1'];
		$this->lettings_app_home_tel_2 = $row['lettings_app_home_tel_2'];
		$this->lettings_app_email = $row['lettings_app_email'];
		$this->lettings_app_dob_ymd = $row['lettings_app_dob_ymd'];
		$this->lettings_app_gender = $row['lettings_app_gender'];
		$this->lettings_app_nationality = $row['lettings_app_nationality'];
		$this->lettings_app_uni_name = $row['lettings_app_uni_name'];
		$this->lettings_app_uni_course = $row['lettings_app_uni_course'];
		$this->lettings_app_uni_student_id = $row['lettings_app_uni_student_id'];
		$this->lettings_app_stay_start_ymd = $row['lettings_app_stay_start_ymd'];
		$this->lettings_app_stay_end_ymd = $row['lettings_app_stay_end_ymd'];
		$this->lettings_app_kin_required = $row['lettings_app_kin_required'];
		$this->lettings_app_kin_first_name = $row['lettings_app_kin_first_name'];
		$this->lettings_app_kin_last_name = $row['lettings_app_kin_last_name'];
		$this->lettings_app_kin_same_address = $row['lettings_app_kin_same_address'];
		$this->lettings_app_kin_add_1 = $row['lettings_app_kin_add_1'];
		$this->lettings_app_kin_add_2 = $row['lettings_app_kin_add_2'];
		$this->lettings_app_kin_add_3 = $row['lettings_app_kin_add_3'];
		$this->lettings_app_kin_add_town = $row['lettings_app_kin_add_town'];
		$this->lettings_app_kin_add_county = $row['lettings_app_kin_add_county'];
		$this->lettings_app_kin_add_postcode = $row['lettings_app_kin_add_postcode'];
		$this->lettings_app_kin_add_country_id = $row['lettings_app_kin_add_country_id'];
		$this->lettings_app_kin_tel_1 = $row['lettings_app_kin_tel_1'];
		$this->lettings_app_kin_tel_2 = $row['lettings_app_kin_tel_2'];
		$this->lettings_app_guarantor_required = $row['lettings_app_guarantor_required'];
		$this->lettings_app_guarantor_first_name = $row['lettings_app_guarantor_first_name'];
		$this->lettings_app_guarantor_last_name = $row['lettings_app_guarantor_last_name'];
		$this->lettings_app_guarantor_relationship = $row['lettings_app_guarantor_relationship'];
		$this->lettings_app_guarantor_email = $row['lettings_app_guarantor_email'];
		$this->lettings_app_guarantor_same_address = $row['lettings_app_guarantor_same_address'];
		$this->lettings_app_guarantor_add_1 = $row['lettings_app_guarantor_add_1'];
		$this->lettings_app_guarantor_add_2 = $row['lettings_app_guarantor_add_2'];
		$this->lettings_app_guarantor_add_3 = $row['lettings_app_guarantor_add_3'];
		$this->lettings_app_guarantor_add_town = $row['lettings_app_guarantor_add_town'];
		$this->lettings_app_guarantor_add_county = $row['lettings_app_guarantor_add_county'];
		$this->lettings_app_guarantor_add_postcode = $row['lettings_app_guarantor_add_postcode'];
		$this->lettings_app_guarantor_add_country_id = $row['lettings_app_guarantor_add_country_id'];
		$this->lettings_app_guarantor_tel_1 = $row['lettings_app_guarantor_tel_1'];
		$this->lettings_app_guarantor_tel_2 = $row['lettings_app_guarantor_tel_2'];
		$this->lettings_app_notes = $row['lettings_app_notes'];
		$this->lettings_app_created_ymdhis = $row['lettings_app_created_ymdhis'];
		$this->lettings_app_transferred_to_intranet_ymdhis = $row['lettings_app_transferred_to_intranet_ymdhis'];
		$this->lettings_app_discon = $row['lettings_app_discon'];
		$this->lettings_app_discon_ymdhis = $row['lettings_app_discon_ymdhis'];
	}	
	
	
	function check_fields($request){
		
		$field = new field;
		$data = new data;
		$error_fields = array();
		
		// Check that application has not already been submitted
		$this->init_check_errors("Your application has already been submitted.");
			if( $this->already_submitted( $request['code'] ) === true ){
				$this->push_check_errors("");
			}
		if( $this->error_fields['save_result'] == "fail" ){return $this->error_fields;}
		
		// Required fields
		$this->init_check_errors("Please complete all the required fields marked with (*).");
			if( $request['first_name_input'] == "" ){ $this->push_check_errors("first_name_label"); }
			if( $request['last_name_input'] == "" ){ $this->push_check_errors("last_name_label"); }
			if( $request['home_address_country_input'] == "" ){ $this->push_check_errors("home_address_country_label"); }
			if( $request['home_address_country_input'] == "15" && $request['home_address_postcode_input'] == "" ){ $this->push_check_errors("home_address_postcode_label"); }
			if( $request['home_address_1_input'] == "" ){ $this->push_check_errors("home_address_1_label"); }
			if( $request['home_address_town_input'] == "" ){ $this->push_check_errors("home_address_town_label"); }
			if( $request['home_dob_input'] == "" ){ $this->push_check_errors("home_dob_label"); }
			if( $request['home_gender_input'] == "" ){ $this->push_check_errors("home_gender_label"); }
			if( $request['home_nation_input'] == "" ){ $this->push_check_errors("home_nation_label"); }
			if( $request['home_email_input'] == "" ){ $this->push_check_errors("home_email_label"); }
			if( $request['home_tel_1_input'] == "" ){ $this->push_check_errors("home_tel_1_label"); }
			if( $request['uni_name_input'] == "" ){ $this->push_check_errors("uni_name_label"); }
			if( $request['uni_course_input'] == "" ){ $this->push_check_errors("uni_course_label"); }
			if( $request['stay_start_date_input'] == "" ){ $this->push_check_errors("stay_start_date_label"); }
			if( $request['stay_end_date_input'] == "" ){ $this->push_check_errors("stay_end_date_label"); }
			
			if( $request['kin_required'] != "Y" ){
				if( $request['kin_first_name_input'] == "" ){ $this->push_check_errors("kin_first_name_label"); }
				if( $request['kin_last_name_input'] == "" ){ $this->push_check_errors("kin_last_name_label"); }
				if( $request['kin_same_address'] != "Y" ){
					if( $request['kin_address_country_input'] == "" ){ $this->push_check_errors("kin_address_country_label"); }
					if( $request['kin_address_country_input'] == "15" && $request['kin_address_postcode_input'] == "" ){ $this->push_check_errors("kin_address_postcode_label"); }
					if( $request['kin_address_1_input'] == "" ){ $this->push_check_errors("kin_address_1_label"); }
					if( $request['kin_address_town_input'] == "" ){ $this->push_check_errors("kin_address_town_label"); }
				}
			}
			
			if( $request['guarantor_required'] == "Y" ){
				if( $request['guarantor_first_name_input'] == "" ){ $this->push_check_errors("guarantor_first_name_label"); }
				if( $request['guarantor_last_name_input'] == "" ){ $this->push_check_errors("guarantor_last_name_label"); }
				if( $request['guarantor_relationship_input'] == "" ){ $this->push_check_errors("guarantor_relationship_label"); }
				if( $request['guarantor_same_address'] != "Y" ){
					if( $request['guarantor_address_country_input'] == "" ){ $this->push_check_errors("guarantor_address_country_label"); }
					if( $request['guarantor_address_country_input'] == "15" && $request['guarantor_address_postcode_input'] == "" ){ $this->push_check_errors("guarantor_address_postcode_label"); }
					if( $request['guarantor_address_1_input'] == "" ){ $this->push_check_errors("guarantor_address_1_label"); }
					if( $request['guarantor_address_town_input'] == "" ){ $this->push_check_errors("guarantor_address_town_label"); }
				}
			}
		if( $this->error_fields['save_result'] == "fail" ){return $this->error_fields;}
		
		// Email format
		$this->init_check_errors("The email address supplied appears to be in the wrong format.");
			if( $request['home_email_input'] != "" && !$field->is_valid_email($request['home_email_input']) ){ $this->push_check_errors("home_email_label"); }
		if( $this->error_fields['save_result'] == "fail" ){return $this->error_fields;}
		
		// Date format
		$this->init_check_errors("Please ensure that dates are entered in the format dd/mm/yyyy.");
			if( !$field->is_valid_date($request['home_dob_input']) ){ $this->push_check_errors("home_dob_label"); }
			if( !$field->is_valid_date($request['stay_start_date_input']) ){ $this->push_check_errors("stay_start_date_label"); }
			if( !$field->is_valid_date($request['stay_end_date_input']) ){ $this->push_check_errors("stay_end_date_label"); }
		if( $this->error_fields['save_result'] == "fail" ){return $this->error_fields;}
		
		// Date order
		$this->init_check_errors("The start date of your stay must be less than the end date.");
			$start_ymd = $data->date_to_ymd($request['stay_start_date_input']);
			$end_ymd = $data->date_to_ymd($request['stay_end_date_input']);
			if( $start_ymd >= $end_ymd ){ $this->push_check_errors("stay_start_date_label");$this->push_check_errors("stay_end_date_label"); }
		if( $this->error_fields['save_result'] == "fail" ){return $this->error_fields;}
		
		$error_fields['save_result'] = "success";
		return $error_fields;	
	}
	function init_check_errors($msg){

		$this->error_fields['field_names'] = array();
		$this->error_fields['save_msg'] = $msg;
	}
	function push_check_errors($field_label){
		
		array_push($this->error_fields['field_names'], $field_label);
		$this->error_fields['save_result'] = "fail";
	}
	
	
	function already_submitted($code=""){
		
		$mysql = new mysql;
		$security = new security;
		
		$sql = "
		SELECT count(*) 
		FROM lettings_apps 
		WHERE 
		lettings_app_online_code IS NOT NULL AND 
		lettings_app_online_code <> '' AND 
		lettings_app_online_code = '".$security->clean_query($code)."' 
		";
		$result = $mysql->query($sql);
		$row = $mysql->fetch_array($result);
		if( $row[0] > 0 ){
			return true;
		}
		
		return false;
	}
	
	
	function save($request){
		
		$mysql = new mysql;
		$security = new security;
		$data = new data;
		
		// Deal with submitted data
		$home_dob_input = $data->date_to_ymd($request['home_dob_input']);
		$stay_start_date_input = $data->date_to_ymd($request['stay_start_date_input']);
		$stay_end_date_input = $data->date_to_ymd($request['stay_end_date_input']);
		
		
		// Convert American format to English
		if($data->ymd_to_date($home_dob_input) == ''){
			$date_str = $request['home_dob_input'];
			$date_parts = explode("/", $date_str);
			$home_dob_input = $date_parts[2].$date_parts[0].$date_parts[1];
		}
		
		if($data->ymd_to_date($stay_start_date_input) == ''){
			$date_str = $request['stay_start_date_input'];
			$date_parts = explode("/", $date_str);
			$stay_start_date_input = $date_parts[2].$date_parts[0].$date_parts[1];
		}
		
		if($data->ymd_to_date($stay_end_date_input) == ''){
			$date_str = $request['stay_end_date_input'];
			$date_parts = explode("/", $date_str);
			$stay_end_date_input = $date_parts[2].$date_parts[0].$date_parts[1];
		}
		
		$kin_same_address = "N";
		if( $request['kin_same_address'] == "Y" ){
			$kin_same_address = "Y";
			$request['kin_address_1_input'] = $request['home_address_1_input'];
			$request['kin_address_2_input'] = $request['home_address_2_input'];
			$request['kin_address_3_input'] = $request['home_address_3_input'];
			$request['kin_address_town_input'] = $request['home_address_town_input'];
			$request['kin_address_county_input'] = $request['home_address_county_input'];
			$request['kin_address_postcode_input'] = $request['home_address_postcode_input'];
			$request['kin_address_country_input'] = $request['home_address_country_input'];
		}
		
		$guarantor_same_address = "N";
		if( $request['guarantor_same_address'] == "Y"){
			$guarantor_same_address = "Y";
			$request['guarantor_address_1_input'] = $request['home_address_1_input'];
			$request['guarantor_address_2_input'] = $request['home_address_2_input'];
			$request['guarantor_address_3_input'] = $request['home_address_3_input'];
			$request['guarantor_address_town_input'] = $request['home_address_town_input'];
			$request['guarantor_address_county_input'] = $request['home_address_county_input'];
			$request['guarantor_address_postcode_input'] = $request['home_address_postcode_input'];
			$request['guarantor_address_country_input'] = $request['home_address_country_input'];
		}
		
		if( $request['kin_required'] == "Y" ){
			$request['kin_first_name_input'] = $request['guarantor_first_name_input'];
			$request['kin_last_name_input'] = $request['guarantor_last_name_input']; 
			$request['kin_address_1_input'] = $request['guarantor_address_1_input'];
			$request['kin_address_2_input'] = $request['guarantor_address_2_input'];
			$request['kin_address_3_input'] = $request['guarantor_address_3_input'];
			$request['kin_address_town_input'] =  $request['guarantor_address_town_input'];
			$request['kin_address_county_input'] = $request['guarantor_address_county_input'];
			$request['kin_address_postcode_input'] = $request['guarantor_address_postcode_input'];
			$request['kin_address_country_input'] = $request['guarantor_address_country_input'];
			$request['kin_address_tel_1_input'] = $request['guarantor_address_tel_1_input'];
			$request['kin_address_tel_2_input'] = $request['guarantor_address_tel_2_input'];
		}
		
		// Web sql
		$sql_main = "
		INSERT INTO lettings_apps SET 
		lettings_app_serial = '".$this->gen_unique_serial('lettings_apps','lettings_app_serial')."',
		lettings_app_input_type_id = 2,
		lettings_app_online_code = '".$security->clean_query($request['code'])."',
		lettings_app_home_first_name = '".$security->clean_query($request['first_name_input'])."',
		lettings_app_home_middle_name = '".$security->clean_query($request['middle_name_input'])."',
		lettings_app_home_last_name = '".$security->clean_query($request['last_name_input'])."',
		lettings_app_home_add_1 = '".$security->clean_query($request['home_address_1_input'])."',
		lettings_app_home_add_2 = '".$security->clean_query($request['home_address_2_input'])."',
		lettings_app_home_add_3 = '".$security->clean_query($request['home_address_3_input'])."',
		lettings_app_home_add_town = '".$security->clean_query($request['home_address_town_input'])."',
		lettings_app_home_add_county = '".$security->clean_query($request['home_address_county_input'])."',
		lettings_app_home_add_postcode = '".$security->clean_query($request['home_address_postcode_input'])."',
		lettings_app_home_add_country_id = ".$security->clean_query($request['home_address_country_input']).",
		lettings_app_home_tel_1 = '".$security->clean_query($request['home_tel_1_input'])."',
		lettings_app_home_tel_2 = '".$security->clean_query($request['home_tel_2_input'])."',
		lettings_app_email = '".$security->clean_query($request['home_email_input'])."',
		lettings_app_dob_ymd = '".$security->clean_query($home_dob_input)."',
		lettings_app_gender = '".$security->clean_query($request['home_gender_input'])."',
		lettings_app_nationality = ".$security->clean_query($request['home_nation_input']).",
		lettings_app_uni_name = '".$security->clean_query($request['uni_name_input'])."',
		lettings_app_uni_course = '".$security->clean_query($request['uni_course_input'])."',
		lettings_app_uni_student_id = '".$security->clean_query($request['uni_student_id_input'])."',
		lettings_app_stay_start_ymd = '".$security->clean_query($stay_start_date_input)."',
		lettings_app_stay_end_ymd = '".$security->clean_query($stay_end_date_input)."',
		lettings_app_kin_required = '".$security->clean_query($request['kin_required'])."',
		lettings_app_kin_first_name = '".$security->clean_query($request['kin_first_name_input'])."',
		lettings_app_kin_last_name = '".$security->clean_query($request['kin_last_name_input'])."',
		lettings_app_kin_same_address = '".$kin_same_address."',
		lettings_app_kin_add_1 = '".$security->clean_query($request['kin_address_1_input'])."',
		lettings_app_kin_add_2 = '".$security->clean_query($request['kin_address_2_input'])."',
		lettings_app_kin_add_3 = '".$security->clean_query($request['kin_address_3_input'])."',
		lettings_app_kin_add_town = '".$security->clean_query($request['kin_address_town_input'])."',
		lettings_app_kin_add_county = '".$security->clean_query($request['kin_address_county_input'])."',
		lettings_app_kin_add_postcode = '".$security->clean_query($request['kin_address_postcode_input'])."',
		lettings_app_kin_add_country_id = ".$security->clean_query($request['kin_address_country_input']).",
		lettings_app_kin_tel_1 = '".$security->clean_query($request['kin_tel_1_input'])."', 
		lettings_app_kin_tel_2 = '".$security->clean_query($request['kin_tel_2_input'])."', 
		lettings_app_guarantor_required = '".$security->clean_query($request['guarantor_required'])."',
		lettings_app_guarantor_first_name = '".$security->clean_query($request['guarantor_first_name_input'])."',
		lettings_app_guarantor_last_name = '".$security->clean_query($request['guarantor_last_name_input'])."',
		lettings_app_guarantor_relationship = '".$security->clean_query($request['guarantor_relationship_input'])."',
		lettings_app_guarantor_email = '".$security->clean_query($request['guarantor_email_input'])."',
		lettings_app_guarantor_same_address = '".$guarantor_same_address."',
		lettings_app_guarantor_add_1 = '".$security->clean_query($request['guarantor_address_1_input'])."',
		lettings_app_guarantor_add_2 = '".$security->clean_query($request['guarantor_address_2_input'])."',
		lettings_app_guarantor_add_3 = '".$security->clean_query($request['guarantor_address_3_input'])."',
		lettings_app_guarantor_add_town = '".$security->clean_query($request['guarantor_address_town_input'])."',
		lettings_app_guarantor_add_county = '".$security->clean_query($request['guarantor_address_county_input'])."',
		lettings_app_guarantor_add_postcode = '".$security->clean_query($request['guarantor_address_postcode_input'])."',
		lettings_app_guarantor_add_country_id = ".$security->clean_query($request['guarantor_address_country_input']).",
		lettings_app_guarantor_tel_1 = '".$security->clean_query($request['guarantor_tel_1_input'])."', 
		lettings_app_guarantor_tel_2 = '".$security->clean_query($request['guarantor_tel_2_input'])."', 
		lettings_app_notes = '".$security->clean_query($request['notes_input'])."',
		lettings_app_created_ymdhis = '".date("YmdHis")."' 
		";		
		$result_main = $mysql->insert($sql_main); 
		if($result_main !== false){
			return true;
		}

		
		return "There was a problem saving this form. Please try again and if this problem persists, please contact us via <a href='&#109;&#97;ilto&#58;%6&#57;&#110;&#102;o&#64;rmgl%&#54;5%74tin%67&#37;73&#46;%&#54;&#51;o&#46;&#117;k'>&#105;nf&#111;&#64;rm&#103;le&#116;ti&#110;&#103;s&#46;co&#46;uk</a>";
	}
	
	
	function get_list($type="", $term=""){
	
		$mysql = new mysql;
		$security = new security;
		
		$clean_term = $security->clean_query($term);
		
		switch( $type ){
			case "1":
				$term_clause = " (lettings_app_home_first_name LIKE '%".$clean_term."%' OR lettings_app_home_last_name LIKE '%".$clean_term."%') AND ";
				break;
			case "2":
				$term_clause = " lettings_app_uni_name LIKE '%".$clean_term."%' AND ";
				break;
			default:
				break;
		}
		
		$sql = "
		SELECT * 
		FROM lettings_apps 
		WHERE 
		".$term_clause." 
		lettings_app_discon = 'N' 
		ORDER BY lettings_app_home_first_name ASC, lettings_app_home_last_name ASC 
		";
		$result = $mysql->query($sql);
		
		return $result;
	}
	
	function get_transfer_list(){
	
		$mysql = new mysql;
		
		$sql = "
		SELECT * 
		FROM lettings_apps 
		WHERE 
		lettings_app_transferred_to_intranet_ymdhis = '' OR lettings_app_transferred_to_intranet_ymdhis IS NULL  
		ORDER BY lettings_app_created_ymdhis ASC 
		";
		$result = $mysql->query($sql);
		
		return $result;
	}
	
	
	function get_fullname(){
		
		$name = $this->lettings_app_home_first_name;
		if( $this->lettings_app_home_middle_name != "" ){
			$name .= " ".$this->lettings_app_home_middle_name;
		}
		$name .= " ".$this->lettings_app_home_last_name;
		
		return $name;
	}
	
	
	function get_address(){
	
		$country = new country;
		$address_array = array();
		
		$address_array[] = trim($this->lettings_app_home_add_1);
		$address_array[] = trim($this->lettings_app_home_add_2);
		$address_array[] = trim($this->lettings_app_home_add_3);
		$address_array[] = trim($this->lettings_app_home_add_town);
		$address_array[] = trim($this->lettings_app_home_add_county);
		$address_array[] = trim($this->lettings_app_home_add_postcode);
		$address_array[] = trim($country->get_country_name($this->lettings_app_home_add_country_id));
		$address_array = array_filter($address_array, "strlen");
		
		return $address_array;
	}
	

	function get_guarantor_address(){
	
		$country = new country;
		$address_array = array();
		
		$address_array[] = trim($this->lettings_app_guarantor_add_1);
		$address_array[] = trim($this->lettings_app_guarantor_add_2);
		$address_array[] = trim($this->lettings_app_guarantor_add_3);
		$address_array[] = trim($this->lettings_app_guarantor_add_town);
		$address_array[] = trim($this->lettings_app_guarantor_add_county);
		$address_array[] = trim($this->lettings_app_guarantor_add_postcode);
		$address_array[] = trim($country->get_country_name($this->lettings_app_guarantor_add_country_id));
		$address_array = array_filter($address_array, "strlen");
		
		return $address_array;
	}
	

	function get_kin_address(){
	
		$country = new country;
		$address_array = array();
		
		$address_array[] = trim($this->lettings_app_kin_add_1);
		$address_array[] = trim($this->lettings_app_kin_add_2);
		$address_array[] = trim($this->lettings_app_kin_add_3);
		$address_array[] = trim($this->lettings_app_kin_add_town);
		$address_array[] = trim($this->lettings_app_kin_add_county);
		$address_array[] = trim($this->lettings_app_kin_add_postcode);
		$address_array[] = trim($country->get_country_name($this->lettings_app_kin_add_country_id));
		$address_array = array_filter($address_array, "strlen");
		
		return $address_array;
	}
	
	
	function update_transferred(){
		
		$mysql = new mysql;
		
		$sql = "
		UPDATE lettings_apps SET
		lettings_app_transferred_to_intranet_ymdhis = '".date("YmdHis")."'
		WHERE 
		lettings_app_id = '".$this->lettings_app_id."'
		";
		$result = $mysql->query($sql);
	}
	
	
	function has_link_expired($encr_ymdhis=""){
		
		if($encr_ymdhis == ""){return true;}
		
		$decr_ymdhis = base64_decode($encr_ymdhis);
		//$decr_ymdhis = $encr_ymdhis;
		
		$now_ymdhis = date("YmdHis");
		if($decr_ymdhis < $now_ymdhis){
			return true;
		}
		
		return false;
	}
		
}

?>