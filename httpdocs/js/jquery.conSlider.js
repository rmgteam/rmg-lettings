(function( $, undefined ) {

	var emptyFunction = function(){};

	$.widget( "ui.conSlider", {
		
		options: {
			element:			$(),				// Object to hold conSlider
			slideClass:			'con-slide',		// Class of each slide
			slideGroup:			'con-slides',		// Class for slide group
			slideContainer:		'con-container',	// Class for slide container
			menuButtonClass:	'con-button',		// Class for menu buttons (up and down)
			menuContainer:		'con-menu',			// Class for menu containter
			menuPosition:		'left',				// Menu position
			orientation:		'landscape',		// orientation of screen
			menuItemHeight:		0,					// Menu item Height
			menuItemWidth:		0,					// Menu item Width
			maxNoPanes:			1,					// Max Number of Panes to show together
			noPanes:			1,					// Max Number of Panes to show together
			menuWidth:			0,					// Menu width variable
			buttonHeight:		0,					// Menu button Height
			buttonWidth:		0,					// Menu button width
			autoPlay:			false,				// Start playing on load
			playInterval:		5000,				// Amount of time in ms until slide changes
			autoPlayTimeout:	$(),				// Variable to hold autoplay timeout
			menuLoop:			true,				// Whether or not the menu loops back to the beginning
			contentLoop:		true,				// Whether or not the content loops back to the beginning
			autoHeight:			true,				// Change height to fit largest content, if false content will be scrollable
			contentAnimation:	'fade',				// Animation for the content
			contentSpeed:		500,				// How long the content change animation takes
			contentDirection:	1,					// 1 = ltr - Left to Right or ttb - Top to Bottom,  2 = rtl - Right to Left or btt - Bottom to Top
			menuSpeed:			400,				// How long the menu paging animation takes
			currentSlide:		0,					// Current slide index
			totalSlides:		1,					// Number of slides
			height:				'auto',				// conSlider Height
			width:				'auto',				// conSlider Width
			windowHeight:		0,					// container Height
			windowWidth:		0,					// container Width
			windowPadding:		0,					// container Padding
			json_url:			'',					// URL to supply JSON
			json_results:		[],					// JSON to build conSlider
			jQuery_ui:			false,				// Whether to style using jQuery UI
			bootstrap_ui:		false,				// Whether to style using Bootstrap
			menuScrollType:		'item',				// move each item or page
			is_moving:			false,				// menu is scrolling
			topBorder:			'',					// Down button top border
			theme:				'conSlider',		// Theme
			devicePixel:		1,					// Width of pixels
		},
		
		_create: function(){
			var self = this;
			
			this.options.element					= this.element;
			
			if(self.options.json_url != ''){
				self._init_source(function(){
					self.build_from_json(self.options.json_results);
				});
			}
			
			self.init();
			
			self._trigger('_create');
		},
		
		rebuild: function(){
			var self = this;
			
			// for initial load
			if(self.options.contentAnimation == 'fade'){
				self.options.element.find('.' + self.options.slideClass).hide();
			}else{
				self.options.element.find('.' + self.options.slideClass).show();
			}
			
			// Get dimesions
			self.options.windowPadding					= self.options.element.parent().outerHeight(true) - self.options.element.parent().height();
			self.options.menuWidth						= self.options.element.find('.' + self.options.menuContainer).outerWidth(true);
			self.options.windowWidth					= self.options.element.parent().width();
			self.options.windowHeight					= self.options.element.parent().height() - self.options.windowPadding;
			
			// get page orientation
			if(window.orientation == 0){
				self.options.orientation = "portrait";
			}else{
				self.options.orientation = "landscape";
			}

			// if the browser is a mobile one
			if( /mobile/i.test(navigator.userAgent) ) {
				// get screen dimensions
				self.options.windowWidth = screen.width;
				self.options.windowHeight = screen.height;
			}
			
			if(self.options.theme == 'conSlider'){
				self.buildThemeConSlider();
			}else if(self.options.theme == 'gallery'){
				self.buildThemeGallery();
			}else if(self.options.theme == 'pane'){
				self.buildThemePane();
			}
			
		},
		
		buildThemeConSlider: function(){
			var self = this;
				
			self.options.element.find('.' + self.options.menuContainer).show();
			
			self.options.element.find('.' + self.options.slideContainer).height(self.options.windowHeight);
						
			if(self.options.windowHeight < ((self.options.buttonHeight * 2) + self.options.menuItemHeight)){
				self.options.windowPadding				= $(window).outerHeight(true) - $(window).height();
				self.options.windowHeight				= $(window).height() - self.options.windowPadding;
			}
			
			var border									= self.options.element.find('.' + self.options.slideContainer).css('border-left-width').replace("px", "") * 2;

			// Set styles on slides for corresponding transition 
			if(self.options.contentAnimation == 'slide'){
			
				var speed_in_s							= (self.options.contentSpeed / 1000);
				
				// Calculate new dimensions
				var slideWidth							= ((self.options.windowWidth - border) - self.options.menuWidth);				
				var containerWidth						= slideWidth * self.options.totalSlides;

				// if transition is slide
				self.options.element.find('.' + self.options.slideClass).css({
					'position'							: 'relative',
					'float'								: 'left',
					'opacity'							: 1,
					'width'								: slideWidth + 'px'
				});
				
				self.options.element.find('.' + self.options.slideGroup).css({
					'-webkit-transition'				: 'all ' + speed_in_s + 's ease-in-out',
					'-moz-transition'					: 'all ' + speed_in_s + 's ease-in-out',
					'-o-transition'						: 'all ' + speed_in_s + 's ease-in-out',
					'transition'						: 'all ' + speed_in_s + 's ease-in-out',
					'overflow-x'						: 'hidden',
					'overflow-y'						: 'hidden',
					'width'								: containerWidth + 'px'
				});
				
				self.options.element.find('.' + self.options.slideClass).css({
					'overflow-x'						: 'hidden',
				});
				
			}else if(self.options.contentAnimation == 'fade'){
				
				// if transition is fade
				self.options.element.find('.' + self.options.slideClass).css({
					'position'							: 'absolute',
					'float'								: 'none',
					'width'								: '100%'
				});
				
				self.options.element.find('.' + self.options.slideGroup).css({
					'transform'							: 'none',
					'width'								: '100%',
					'overflow-x'						: 'hidden'
				});
				
				self.options.element.find('.' + self.options.slideClass).eq(0).css('opacity', 1);
				
			}
				
			var button_height							= self.options.buttonHeight * 2;
			var li_height								= button_height;
			
			if(self.options.autoHeight){
				// Calculate height
				var slideHeight							= 0;
				
				self.options.element.find('.' + self.options.slideClass).each(function () {
					if(slideHeight < $(this).innerHeight()){
						slideHeight						=$(this).outerHeight();
					}
				});
	
				var containerHeight						= self.options.element.find('.' + self.options.slideContainer).innerHeight();

				self.options.element.find('.' + self.options.menuContainer + ' ul li a').each(function () {
					//if(li_height < slideHeight){
						li_height						+= $(this).outerHeight();
					/*}else{
						return;
					}*/
				});
				
				var overflow_y							= 'visible';
				
				self.options.height						= li_height;
				
			}else{
				li_height								+= self.options.height;
				
				var overflow_y							= 'scroll';
			}
			
			var ul 										= self.options.element.find('.' + self.options.menuContainer + ' ul');
			
			if(li_height <= self.options.windowHeight){
				self.options.element.find('.' + self.options.menuButtonClass).hide();
				li_height 								= self.options.windowHeight;
				ul.find('li:first a').css('border-top-width', '1px');
				ul.css({
					'height'							: 'auto'
				});
				var ul_height 							= ul.height();
				ul.css('top', ((li_height - ul_height) / 2) + 'px');
			}else{
				self.options.element.find('.' + self.options.menuButtonClass).show();
				while(li_height > self.options.windowHeight){
					li_height							= li_height - self.options.menuItemHeight;
					overflow_y							= 'scroll';
				}
				
				ul.find('li:first a').css('border-top-width', '0');
				
				ul.css({
					'height'								: li_height - button_height + 'px',
					'top'									: '0'
				});
			}
			
			// Set styles on slides for corresponding transition 
			if(self.options.contentAnimation == 'slide'){
				
				self.options.element.find('.' + self.options.slideClass).css({
					'overflow-y'						: overflow_y
				});
				
			}else if(self.options.contentAnimation == 'fade'){
				
				self.options.element.find('.' + self.options.slideGroup).css({
					'overflow-y'						: overflow_y
				});		
						
			}
			
			// Set Height
			self.options.element.find('.' + self.options.menuContainer).css('height', li_height + 'px');
			self.options.element.find('.' + self.options.slideContainer).css('height', li_height + 'px');
			
			if(self.options.menuPosition == 'left'){
				self.options.element.find('.' + self.options.menuContainer)
					.removeClass('right');
					
				self.options.element.find('.' + self.options.slideContainer)
					.removeClass('right');
			}else if(self.options.menuPosition == 'right'){
				self.options.element.find('.' + self.options.menuContainer)
					.addClass('right');
					
				self.options.element.find('.' + self.options.slideContainer)
					.addClass('right');
			}
			
			var	full_menu_height						= self.options.menuItemHeight * self.options.totalSlides;
			
			if(full_menu_height < li_height){
				self.options.element.find('.' + self.options.menuButtonClass + '.last a').css('border-top-width', '0');
			}else{
				self.options.element.find('.' + self.options.menuButtonClass + '.last a').css('border-top-width', self.options.topBorder);
			}
			
			// Reload current slide
			self.goTo(self.options.currentSlide);
			
		},
		
		buildThemeGallery: function(){
			var self = this;
			
			self.options.element.find('.' + self.options.slideContainer).height(self.options.windowHeight);
			
			// Set Menu position
			var ulContainer									= self.options.element.find('.' + self.options.menuContainer);
			
			ulContainer.show();
			
			// If the conSlider defaults weren't changed for gallery
			if(self.options.menuPosition == 'left'){
				self.options.menuPosition 				= 'bottom';
			}else if(self.options.menuPosition == 'right'){
				self.options.menuPosition 				= 'top';
			}
			
			if(self.options.menuPosition == 'bottom'){
				ulContainer
					.detach()
					.removeClass('left right top')
					.addClass('bottom')
					.appendTo(self.options.element);
			}else if(self.options.menuPosition == 'top'){
				ulContainer
					.removeClass('left right bottom')
					.addClass('top');
			}
			
			var border									= self.options.element.find('.' + self.options.slideContainer).css('border-left-width').replace("px", "") * 2;
				
			// Set styles on slides for corresponding transition 
			if(self.options.contentAnimation == 'slide'){
			
				var speed_in_s							= (self.options.contentSpeed / 1000);
				
				// Calculate new dimensions
				var slideWidth							= (self.options.windowWidth - border);				
				var containerWidth						= slideWidth * self.options.totalSlides;

				// if transition is slide
				self.options.element.find('.' + self.options.slideClass).css({
					'position'							: 'relative',
					'float'								: 'left',
					'opacity'							: 1,
					'width'								: slideWidth + 'px'
				});
				
				self.options.element.find('.' + self.options.slideGroup).css({
					'-webkit-transition'				: 'all ' + speed_in_s + 's ease-in-out',
					'-moz-transition'					: 'all ' + speed_in_s + 's ease-in-out',
					'-o-transition'						: 'all ' + speed_in_s + 's ease-in-out',
					'transition'						: 'all ' + speed_in_s + 's ease-in-out',
					'overflow-x'						: 'hidden',
					'overflow-y'						: 'hidden',
					'width'								: containerWidth + 'px'
				});
				
				self.options.element.find('.' + self.options.slideClass).css({
					'overflow'							: 'hidden'
				});
				
			}else if(self.options.contentAnimation == 'fade'){
				
				// if transition is fade
				self.options.element.find('.' + self.options.slideClass).css({
					'position'							: 'absolute',
					'float'								: 'none',
					'width'								: '100%'
				});
				
				self.options.element.find('.' + self.options.slideGroup).css({
					'transform'							: 'none',
					'width'								: '100%',
					'overflow'							: 'hidden'
				});
				
				self.options.element.find('.' + self.options.slideClass).eq(0).css('opacity', 1);
				
			}
				
			var button_width							= 0;
			self.options.element.find('.' + self.options.menuButtonClass + ' a').each(function(){
				button_width							+=  $(this).outerWidth(true);
			});
			
			var ul 										= self.options.element.find('.' + self.options.menuContainer + ' ul');
			var li_height								= 0;
			
			self.options.element.find('.' + self.options.menuContainer + ' ul li a').each(function () {
				li_height								= $(this).outerHeight(true);
				return;
			});
				
			self.options.element.find('.' + self.options.menuButtonClass).hide();
			
			ul.css({
				'width'									: '100%',
				'overflow'								: 'auto'
			});
			
			if(ul.get(0).scrollHeight <= ul.height()){
				li_width 								= self.options.windowWidth;
			}else{
				self.options.element.find('.' + self.options.menuButtonClass).show();
				
				li_width 								= self.options.element.find('.' + self.options.slideContainer).width() -1;
				
				ul.css({
					'width'								: li_width - button_width + 'px',
					'overflow'							: 'hidden'
				});
			}
			
			self.options.element.find('.' + self.options.menuContainer + ' ul li a').each(function () {
				var anchorElement							= $(this);
				var id										= $(this).attr('href').replace('#', '');
				self.options.element.find('.' + self.options.slideClass).each(function(){
					var result = $(this).data('slide-id');
					if (result == id){
						var thisSlide						= $(this);
						anchorElement.find('span').detach().appendTo(thisSlide.find('div'));
					}
				});
			});
			
			var li_height								= self.options.height;

			if(self.options.autoHeight){
	
				var menuHeight							= self.options.element.find('.' + self.options.menuContainer).outerHeight(true);

				li_height								= self.options.windowHeight - menuHeight;
				
				self.options.height						= li_height;
				
			}
			
			// Set Height
			self.options.element.find('.' + self.options.slideContainer).css('height', li_height + 'px');
			
			// Reload current slide
			self.goTo(self.options.currentSlide);
			
		},
		
		buildThemePane: function(){
			var self = this;
				
			self.options.element.find('.' + self.options.menuContainer).hide();
			
			self.options.windowHeight					= self.options.element.parent().height();
			
			// if the browser is a mobile one
			if( /mobile/i.test(navigator.userAgent) ) {
				// get screen dimensions
				self.options.windowHeight = screen.height;
			}
			
			// Get Content Width
			var cWidth 									= 0;
			self.options.element.find('.' + self.options.slideClass).eq(0).children().each(function(){
				cWidth									+= $(this).width();
			});
				
			// Calculate new dimensions
			var border									= self.options.element.find('.' + self.options.slideContainer).css('border-left-width').replace("px", "") * 2;
			var outerWidth								= ((self.options.windowWidth - (self.options.buttonWidth * 2)) - border) * self.options.devicePixel;
			var slideWidth								= (((self.options.windowWidth - (self.options.buttonWidth * 2)) - border) / self.options.maxNoPanes) * self.options.devicePixel;	
			
			// Find the number of panes that will fit in the viewport
			self.options.noPanes						= self.options.maxNoPanes;
			if(cWidth > slideWidth){
				var x									= self.options.maxNoPanes;
				while(x > 0){
					slideWidth							= ((self.options.windowWidth - (self.options.buttonWidth * 2)) - border) / x;
					if( /mobile/i.test(navigator.userAgent) ) {
						slideWidth								= (slideWidth * self.options.devicePixel);
					}
					self.options.noPanes				= x;
					
					if(cWidth <= slideWidth){
						break;
					}
					x --;
				}
			}	
			
			var containerWidth							= slideWidth * self.options.totalSlides;

			// Set styles on slides for corresponding transition 
			if(self.options.contentAnimation == 'slide'){
			
				var speed_in_s							= (self.options.contentSpeed / 1000);

				// if transition is slide
				self.options.element.find('.' + self.options.slideClass).css({
					'position'							: 'relative',
					'float'								: 'left',
					'opacity'							: 1,
					'width'								: slideWidth + 'px'
				});
				
				self.options.element.find('.' + self.options.slideGroup).css({
					'-webkit-transition'				: 'all ' + speed_in_s + 's ease-in-out',
					'-moz-transition'					: 'all ' + speed_in_s + 's ease-in-out',
					'-o-transition'						: 'all ' + speed_in_s + 's ease-in-out',
					'transition'						: 'all ' + speed_in_s + 's ease-in-out',
					'overflow-x'						: 'hidden',
					'overflow-y'						: 'hidden',
					'width'								: containerWidth + 'px',
					'transform'							: "translateX(0px)"
				});
				
				self.options.currentSlide				= 0;
				
				self.options.element.find('.' + self.options.slideClass).css({
					'overflow-x'						: 'hidden',
				});
				
			}else if(self.options.contentAnimation == 'fade'){
				
				// if transition is fade
				self.options.element.find('.' + self.options.slideClass).css({
					'position'							: 'absolute',
					'float'								: 'none',
					'width'								: slideWidth + 'px'
				});
				
				self.options.element.find('.' + self.options.slideGroup).css({
					'transform'							: 'none',
					'width'								: '100%',
					'overflow-x'						: 'hidden'
				});
				
				self.options.element.find('.' + self.options.slideClass).eq(0).css('opacity', 1);
				
			}
				
			self.options.element.find('.' + self.options.slideContainer).css('width', outerWidth + 'px');
				
			var button_height							= self.options.buttonHeight;
			var li_height								= 0;

			if(self.options.autoHeight){
				
				var cHeight 							= 0;
				self.options.element.find('.' + self.options.slideClass).eq(0).children().each(function(){
					cHeight								+= $(this).height();
				});
				
				self.options.element.find('.' + self.options.slideClass).each(function(){
					var tHeight									= 0;
					$(this).children().each(function(){
						tHeight									+= $(this).width();
					});
					
					if(tHeight > cHeight){
						cHeight 								= tHeight;
					}
				});
				
				// Calculate height
				var slideHeight							= 0;
				
				self.options.element.find('.' + self.options.slideClass).each(function () {
					if(slideHeight < $(this).innerHeight()){
						slideHeight							=$(this).outerHeight();
					}
				});
				
				var containerHeight						= self.options.element.find('.' + self.options.slideContainer).innerHeight();
				
				if(slideHeight > cHeight){
					slideHeight							= cHeight;
				}

				li_height								= slideHeight;
				
				var overflow_y							= 'visible';
				
				self.options.height						= li_height;
				
			}else{
				li_height								+= self.options.height;
				
				var overflow_y							= 'scroll';
			}
			
			// Set styles on slides for corresponding transition 
			if(self.options.contentAnimation == 'slide'){
				
				self.options.element.find('.' + self.options.slideClass).css({
					'overflow-y'						: overflow_y
				});
				
			}else if(self.options.contentAnimation == 'fade'){
				
				self.options.element.find('.' + self.options.slideGroup).css({
					'overflow-y'						: overflow_y
				});		
						
			}
			
			// Set Height
			self.options.element.find('.' + self.options.slideContainer).css({
				'width': outerWidth + 'px'
			});
			
			self.options.element.find('.' + self.options.menuButtonClass).css('margin-top', ((li_height - button_height)/ self.options.devicePixel )/2 + 'px');
			
			// Hide buttons is neccessary
			if(self.options.totalSlides == self.options.noPanes){
				self.options.element.find('.' + self.options.menuButtonClass).css('visibility', 'hidden');
			}else{
				self.options.element.find('.' + self.options.menuButtonClass).css('visibility', 'visible');
			}
			
			// Reload current slide
			self.goTo(self.options.currentSlide);
			
		},
		
		init: function(){
			var self = this;	
			
			if(window.devicePixelRatio!=undefined){
				self.options.devicePixel				= window.devicePixelRatio;
			}
			
			// Set correct css class
			if(self.options.theme == 'conSlider'){
				self.options.menuScrollType 			= 'item';
				self.options.element
					.removeClass("gallery pane")
					.addClass("conSlider");
			}else if(self.options.theme == 'gallery'){
				self.options.menuScrollType 			= 'item';
				self.options.element
					.removeClass("conSlider pane")
					.addClass("gallery");
			}else if(self.options.theme == 'pane'){
				self.options.menuScrollType 			= 'slide';
				self.options.element
					.removeClass("gallery conSlider")
					.addClass("pane");
					
				if(self.options.contentAnimation == "fade" && self.options.maxNoPanes > 1){
					self.options.contentAnimation		= 'slide';	
				}
			}
			
			// Get number of slides
			self.options.totalSlides 				= self.options.element.find('.' + self.options.slideClass).length;
			self.options.menuItemHeight				= self.options.element.find('.' + self.options.menuContainer + ' ul li a').outerHeight();
			
			// Set onclick event on menu
			self.options.element.find('.' + self.options.menuContainer + ' ul li a').each(function () {
				$(this).on('click', {'self' : self}, function(e){
					e.preventDefault();
					var self 						= e.data['self'];
					var id 							= $(this).attr('href').replace('#', '');
					self.jumpTo(id);
				});
			});
			
			// Create up button
			var div									= $( document.createElement('div') )
													.addClass(self.options.menuButtonClass);
			var a									= $( document.createElement('a') )
													.attr('href', '#')
													.attr('id', 'con-button-up')
													.addClass('arrow')
													.on('click', {'self':self}, function(e){
														e.preventDefault();
														var self	= e.data['self'];
														if(self.options.theme == 'conSlider'){
															self.menuScroll('up');
														}else{
															self.menuScroll('left');
														}
													});
			var up_arrow;
			
			if(self.options.jQuery_ui){
				var icon 							= 'n';
				
				if(self.options.theme != 'conSlider'){
					icon 							= 'w';
				}
				
				up_arrow							= $( document.createElement('span') )													.addClass('ui-icon ui-icon-carat-1-' + icon);
			}else if(self.options.bootstrap_ui){
				var icon 							= 'up';
				
				if(self.options.theme != 'conSlider'){
					icon 							= 'left';
				}
				
				up_arrow							= $( document.createElement('span') )
													.addClass('glyphicon glyphicon-chevron-' + icon)
													.css('width', '100%');
			}else{
				var icon 							= 'n';
				
				if(self.options.theme != 'conSlider'){
					icon 							= 'w';
				}
				
				up_arrow							= $( document.createElement('span') )
													.addClass('ui-icon ui-icon-carat-1-' + icon);
			}
			
			a.append(up_arrow)
			div.append(a);
			
			if(self.options.theme == "pane"){
				self.options.element.find('.' + self.options.slideContainer).parent().prepend(div);
			}else{
				self.options.element.find('.' + self.options.menuContainer).prepend(div);
			}
			
			// Create down button
			var div									= $( document.createElement('div') )
													.addClass(self.options.menuButtonClass + ' last');
			var a									= $( document.createElement('a') )
													.attr('href', '#')
													.attr('id', 'con-button-down')
													.addClass('arrow')
													.on('click', {'self':self}, function(e){
														e.preventDefault();
														var self	= e.data['self'];
														
														if(self.options.theme == 'conSlider'){
															self.menuScroll('down');
														}else{
															self.menuScroll('right');
														}
													});
			
			var down_arrow;
			
			if(self.options.jQuery_ui){
				var icon 							= 's';
				
				if(self.options.theme != 'conSlider'){
					icon 							= 'e';
				}
				
				down_arrow							= $( document.createElement('span') )
													.addClass('ui-icon ui-icon-carat-1-' + icon);
			}else if(self.options.bootstrap_ui){
				var icon 							= 'down';
				
				if(self.options.theme != 'conSlider'){
					icon 							= 'right';
				}
				
				down_arrow							= $( document.createElement('span') )
													.addClass('glyphicon glyphicon-chevron-' + icon)
													.css('width', '100%');
			}else{
				var icon 							= 's';
				
				if(self.options.theme != 'conSlider'){
					icon 							= 'e';
				}
				
				down_arrow							= $( document.createElement('span') )
													.addClass('ui-icon ui-icon-carat-1-' + icon);
			}
			
			a.append(down_arrow)
			div.append(a);
			
			if(self.options.theme == "pane"){
				self.options.element.find('.' + self.options.slideContainer).parent().append(div);
			}else{
				self.options.element.find('.' + self.options.menuContainer).append(div);
			}
			
			self.options.buttonHeight				= self.options.element.find('.' + self.options.menuButtonClass).eq(0).outerHeight();
				
			self.options.element.find('.' + self.options.menuButtonClass + ' a').each(function(){
				self.options.buttonWidth				=  $(this).outerWidth(true); 
				return;
			});

			// Set styles on menu
			var index								= 0;
			
			self.options.element.find('.' + self.options.menuContainer + ' ul li').each(function () {
				
				if(self.options.jQuery_ui){
					$(this).find('a')
						.addClass('ui-state-default')
						.button()
						.removeClass('ui-corner-all');
				}else if(self.options.bootstrap_ui){
					$(this).find('a')
						.addClass('btn');
				}
				
				index ++;
			});
			
			self.options.element.find('.' + self.options.menuContainer + ' ul li a').each(function () {
				
				// If side menu is only an image, turn it into a div with background, for scaling
				var src 							= ''; 
				var li								= $(this);
				if($(this).children().length == 1){
					$(this).children().each(function(){
						if($(this).prop("tagName") == 'IMG'){
							src 					= $(this).attr('src');
	
							$(this).remove();
							var contentDiv			= $(document.createElement('div'))
													.css({
														'background-image'		: 'url(' + src + ')',
														'background-size'		: 'contain',

														'background-repeat'		: 'no-repeat',
														'width'					: '100%',
														'height'				: '100%'
													})
													.appendTo(li);
						}
					});
				}
			});
			
			self.options.element.find('.' + self.options.slideClass).each(function () {
				// If slide is only an image, turn it into a div with background, for scaling
				var src 							= ''; 
				var li								= $(this);
				if($(this).children().length == 1){
					$(this).children().each(function(){
						if($(this).prop("tagName") == 'IMG'){
							src 					= $(this).attr('src');
	
							$(this).remove();
							var contentDiv			= $(document.createElement('div'))
													.css({
														'background-image'		: 'url(' + src + ')',
														'background-size'		: 'contain',
														'background-repeat'		: 'no-repeat',
														'background-position'	: 'center center',
														'width'					: '100%',
														'height'				: '100%'
													})
													.appendTo(li);
						}
					});
				}
			});
			
			self.options.topBorder					= self.options.element.find('.' + self.options.menuButtonClass + '.last').css("border-top-width");
			self.options.element.find('.' + self.options.menuButtonClass + '.last a').css("border-top-width", 0);
			
			// Add resize event
			$(window).on('resize', {'self' : self}, function(e){
				var self 						= e.data['self'];
				self.rebuild();
			});
			
			self.rebuild();
			
			// Set auto play going if set
			if(self.options.autoPlay){
				self.play();
			}
			
			self._trigger('_init');
			
		},

		_init_source: function(callback) {
			var self = this;
			
			// If json url isn't blank
			if ( self.options.json_url != '') {

				// Get JSON
				$.get(self.options.json_url, 
				{},
				function(data){
					
					// Build object
					var arr 						= $.makeArray(data);
					self.options.json_results 		= arr;

					if($.isFunction(callback) === true){
						callback();
					}
				}, 
				"json");
			}
			
			self._trigger('_init_source');
		},

		build_from_json: function (data) {
			var self = this;

			if (typeof(data) !== 'object') return;
			
			// Build menu container
			var menu 								= $( document.createElement('div') )
													.addClass(self.options.menuContainer);
			
			// Build slide Group
			var slides 								= $( document.createElement('div') )
													.addClass(self.options.slideGroup);

			// Start list
			var list 								= $(document.createElement('ul'));

			$.each(data, function(index, item) {
				
				if(item.title != ''){
					
					// Build Anchor
					var listAnchor					= $(document.createElement('a'))
													.attr('href', '#' + item.title)
													.html(item.title);
					
					// Build li
					var listItem 					= $(document.createElement('li'))
													.append(listAnchor);
					
					list.append(listItem);
					
					// Build Content
					var contentDiv					= $(document.createElement('div'))
													.addClass(self.options.slideClass)
													.attr('data-slide-id', item.title)
													.html(item.html)
													.appendTo(slides);
				}
			});
			
			var items 								= $( document.createElement('div') )
													.addClass(self.options.menuItemsClass);
			
			list.appendTo(items);
			items.appendTo(menu);
			menu.appendTo(self.options.element)
			
			// Build slide containter
			var slideContainer 						= $( document.createElement('div') )
													.addClass(self.options.slideContainer);
			
			slides.appendTo(slideContainer);
			
			slideContainer.appendTo(self.options.element);
			
			self._trigger('_build_from_json');
		},
		
		get_slide_index: function(){
			var self = this;
			
			self._trigger('_get_slide_index');
			
			return self.options.currentSlide;
		},
		
		get_transition: function(){
			var self = this;
			
			self._trigger('_get_transition');
			
			return self.options.contentAnimation;
		},
		
		play: function(){
			var self = this;
			
			window.clearInterval(self.options.autoPlayTimeout);
			
			self.options.autoPlayTimeout = window.setInterval(function () {
				self.options.next();
			}, self.options.options.autoPlay);
			
			self._trigger('_play');
		},

		stop: function(){
			var self = this;
			
			window.clearInterval(self.options.autoPlayTimeout);
			
			self._trigger('_stop');
		},
		
		next: function () {
			var self = this;
			
			var nextSlide = Math.min(self.options.totalSlides ,(self.options.currentSlide + 1));
			
			if(self.options.theme == 'pane'){
				var x								= self.options.noPanes - 1;
				while(x > 0){	
					if(nextSlide == (self.options.totalSlides - x)){
						nextSlide					= self.options.totalSlides - (x - (self.options.noPanes + 1));
					}
					
					x --;
				}
			}
			
			if(self.options.contentLoop === true){
				if(nextSlide >= self.options.totalSlides){
					nextSlide 						= 0;
				}
			}
			
			self.goTo(nextSlide);
			
			self._trigger('_next');
		},

		prev: function () {
			var self = this;
			
			var prevSlide = Math.max(-1,(self.options.currentSlide - 1));
			
			if(self.options.contentLoop === true){
				if(prevSlide < 0){
					prevSlide 						= self.options.totalSlides - 1;
				}
			}
			
			if(self.options.theme == 'pane'){
				var x								= self.options.noPanes - 1;
				while(x > 0){	
					if(prevSlide == (self.options.totalSlides - x)){
						prevSlide					= self.options.totalSlides + (x - (self.options.noPanes + 1));
					}
					
					x --;
				}
			}
			
			self.goTo(prevSlide);

			self._trigger('_prev');
		},
		
		goTo: function (position) {
			var self = this;
			
			self.options.currentSlide 				= position;

			var width								= self.options.element.find('.' + self.options.slideContainer).width();
			
			if(self.options.theme == 'pane'){
				width								= self.options.element.find('.' + self.options.slideClass).outerWidth();
			}
			
			if(self.options.contentAnimation == 'slide'){
				
				var amount							= position * (-width);
				
				// if transition is slide
				self.options.element.find('.' + self.options.slideGroup).css("transform","translateX("+amount+"px)");
				
			}else if(self.options.contentAnimation == 'fade'){
				// if transition is fade				
				self.options.element.find('.' + self.options.slideClass).not(':eq(' + self.options.currentSlide + ')').hide('fade', self.options.contentSpeed);
				self.options.element.find('.' + self.options.slideClass).eq(self.options.currentSlide).show('fade', self.options.contentSpeed);
				
			}
			
			var id									= self.options.element.find('.' + self.options.slideClass).eq(self.options.currentSlide).data('slide-id');
			
			self.options.element.find('li').removeClass('active');
			
			self.options.element.find('a[href="#' + id + '"]').parent().addClass('active');
			
			self._trigger('_goTo');
        },
        
        jumpTo: function (id){
    		var self = this;
			
			var position;
			
			var i									= 0;
			
			self.options.element.find('.' + self.options.slideClass).each(function(){
				var result = $(this).data('slide-id');
			    if (result == id){
			    	position 						= i;
			    }
			    i++;
			});
			
			self.goTo(position);
			
			self._trigger('_jumpTo');
        },

		setOption: function(key, value){

			$.Widget.prototype._setOption.apply( this, arguments );
		},
        
		menuScroll: function (dir){

    		var self = this;
			
			self.options.menuItemWidth 				= self.options.element.find('.' + self.options.menuContainer + ' ul li a').outerWidth(true);
    		
    		// If the menu is to move an item at a time
    		if(self.options.menuScrollType == 'item'){
    			// Get list
    			var ul 								= self.options.element.find('.' + self.options.menuContainer + ' ul');
    			
    			// If not already moving
    			if(!self.options.is_moving){
    				// Set we are moving
    				self.options.is_moving			= true;
    				
	    			if(dir == 'down'){
	    				
	    				 var element = ul.children('li:first');
	    				 
                         element
	                         .animate({'marginTop': '-' + self.options.menuItemHeight + 'px'}, self.options.menuSpeed, function(){
                                 element
                                 	.detach()
                                 	.css('marginTop', '0')
                                 	.appendTo(ul);
                                 self.options.is_moving	= false;
	                         }.bind(this));
	    				
	    			}else if(dir == 'up'){
	    				
	    				ul.children('li:last')
	    					.detach()
	    					.prependTo(ul)
	    					.css('marginTop', '-' + self.options.menuItemHeight + 'px')
	    					.animate({'marginTop': '0px'}, self.options.menuSpeed, function(){
	    						self.options.is_moving	= false;
	    					}.bind(this));
							
	    			}else if(dir == 'left'){
	    				var element = ul.children('li:first');
	    				 
                         element
	                         .animate({'marginLeft': '-' + self.options.menuItemWidth + 'px'}, self.options.menuSpeed, function(){
                                 element
                                 	.detach()
                                 	.css('marginLeft', '0')
                                 	.appendTo(ul);
                                 self.options.is_moving	= false;
	                         }.bind(this));
					
					}else if(dir == 'right'){
	    				
	    				ul.children('li:last')
	    					.detach()
	    					.prependTo(ul)
	    					.css('marginLeft', '-' + self.options.menuItemWidth + 'px')
	    					.animate({'marginLeft': '0px'}, self.options.menuSpeed, function(){
	    						self.options.is_moving	= false;
	    					}.bind(this));
	    			}
    			}

    		}else{
    			if(dir == 'left'){
					self.prev();
				}else if(dir == 'right'){
					self.next();
				}
    		}
		}
		
	});
}( jQuery ) );