<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="RMG Lettings" />
<meta name="description" content="RMG Lettings" />
<meta name="keywords"  content="student,homes,manchester,lettings" />
<meta name="Resource-type" content="Document" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>RMG Lettings</title>


<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="css/conSlider.css" media="screen" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" charset="utf-8" href="css/gothambold.css" />
<link rel="stylesheet" type="text/css" charset="utf-8" href="css/gotham.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css" />
<link href="css/themes_smoothness_jquery-ui.css" rel="stylesheet" type="text/css" media="screen" /> 
<link href="css/common.css" rel="stylesheet" type="text/css" media="screen" />


<!--[if IE]>
<script type="text/javascript">
var console = { log: function() {} };
</script>
<![endif]-->

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="js/jquery.fullPage.min.js"></script>
<script type="text/javascript" src="js/jquery.conSlider.js"></script> 

<script type="text/javascript">
$(document).ready(function() {
$('#fullpage').fullpage({
		anchors: ['Home', 'Info', 'Contact Us','Student Life','Gallery','Video','Test Contact'],
		sectionsColor: ['', '#FCF1BB', '','#FCF1BB','', '#FCF1BB'],
		menu:'#mymenu',
		scrollOverflow: true,
		verticalCentered:false,
		resize:false,
			afterLoad: function(anchor,index){
			/*
			$('#nav_'+index).addClass('active')
				.siblings().removeClass('active'); */
				/* if(index == 3) {
				$('#contact').css({
				'overflow-x'						: 'hidden',
				 'overflow-y'						: 'scroll'
				});
				}   */
				if(index == 6)  {
					$('#montgomery_house').attr('src','http://www.youtube.com/embed/dPmIXJsMZlw?autoplay=1');
				} else {
					$('#montgomery_house').attr('src','http://www.youtube.com/embed/dPmIXJsMZlw');
			            }
		    },
		    afterResize: function(){ 
               $('#lettings_slider').conSlider('rebuild');
                $('#student_slider').conSlider('rebuild');
                $('#gallery_slider').conSlider('rebuild');
			}
				    
			});

			$(".date").datepicker({dateFormat : "dd/mm/yy", changeYear : true, minDate : "+0D"});

				//conslider
			  $('#lettings_slider').conSlider({
				 bootstrap_ui : true
				});
				$('#student_slider').conSlider({
					bootstrap_ui : true
				});
				$('#gallery_slider').conSlider({
					bootstrap_ui : true
				}); 
});
//make an enquire page form submission
function send_form(){
document.form1.submit();
}

		/****Responsive  Sticky Header ****/
$(function() {var $document = $(document),
$element = $('#headerContainer'),
className = 'stickyNav';
$document.scroll(function() {if ($document.scrollTop() >= 300) {
$element.addClass("stickyNav" );}
else {$element.removeClass("stickyNav");}});});



</script>
<? require_once("utils.php");?>
<? require_once("includes/analytics.php");?>
</head>
<body>
<?php include_once 'includes/header.php';?>
<div id="fullpage">
	<div class="section " id="section0">
		<div class="container" id="headerbg">
			<div class="row">
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 ">
				  	<img class="img-responsive logo" src="/images/logo.png"/>
				</div>
				<div class="pricebadge col-xs-2 col-sm-2 col-md-2 col-lg-2">
			  		<img class="img-responsive" src="../images/price-badge.png"/>
			  	</div>
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 approved">
					<img class="img-responsive" src="../images/approved.png"/>
				</div>
			</div>
			
			<div class="row">
			  <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 paddleft25 wel-txt-div">
			  		<p class="yellowfont fontbg">Welcome to<br/></p>
			  		<p class="whitefontgotham fontbg">RMG Lettings<br/></p>
			  		<p class="whitefont fontmd">RMG Lettings can assist with your Manchester student accommodation needs.  <br/>
			  		Our Manchester Student Halls  - Monty Halls - are suitable for local, international, ERASMUS exchange students, or language school students looking for a short term let, long term let, or single semester.</p>
			  	</div>
			</div>
			<div class="row">			
			  	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-superlg-6 yellowfont fontmd paddleft25">
			  			<br/><br/>
			  			To book a room or for any further enquiries please fill in the <a href="#Contact Us" class="yellowfont fontlg">enquiry form</a>.<br/> 
						Alternatively you can contact us on <span class="fontlg"><?php echo $UTILS_TEL_LETTINGS_MAIN;?></span> or email us at <a class="yellowfont fontlg"href='&#109;&#97;i&#108;&#116;o&#58;in&#37;&#54;6o&#64;r&#109;g%6C&#101;&#37;&#55;&#52;%7&#52;in&#103;%73&#46;c&#111;&#46;&#117;k'>info&#64;rmglett&#105;&#110;gs&#46;&#99;o&#46;uk</a>
			  	
			  		<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
			  	</div>
			  	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-superlg-2">
			  		<a href="#Contact Us"><div class=" pull-left btn-custom btn-lg fontmd">Enquire Now</div></a>
			  	</div>
			  	
			</div>
			
		</div>
	</div>
	
	<div class="section" id="section1">
	<div class="body-conslider">
	   <div id="lettings_slider" class="conSlider">
	   
			<div class="con-menu">
				<ul>
					<li><a href="#facilities"><span class="bluefontgotham fontmd">Facilities</span></a></li>
					<li><a href="#rates"><span class="bluefontgotham fontmd">Rates</span></a></li>
					<li><a href="#details"><span class="bluefontgotham fontmd">Details</span></a></li>
					<li><a href="#amenities"><span class="bluefontgotham fontmd">Amenities</span></a></li>
					<li><a href="#location"><span class="bluefontgotham fontmd">Location</span></a></li>
					<li><a href="#what_to_bring"><span class="bluefontgotham fontmd">What to bring</span></a></li>
				</ul>
			</div>
			
			<div  class="con-container">
				<div class="con-slides"> 
				
					<!-- <div class="con-slide" data-slide-id="affordable_accomodation">
						<span class="bluefont">Affordable Accommodation</span>
						<br/>
						 <p>RMG Lettings can provide you with quality, affordable accommodation from &pound;75 per week rent which includes all of your Bills and Internet access.</p>
		                    
		                    <h3>Contact us</h3>
		                    <p>For more information on Montgomery House click on the <a class="lbluefont" href="#Student Life">Student Life</a> on the above menu. To book a room or for any further enquiries please fill in the 
		                    <a href="#Contact Us" class="lbluefont">enquiry form</a>.  Alternatively you can contact us on <span class="lbluefont"><?=$UTILS_TEL_LETTINGS_MAIN;?></span> or 
		                    email us at <a class="lbluefont" href='&#109;&#97;i&#108;&#116;o&#58;in&#37;&#54;6o&#64;r&#109;g%6C&#101;&#37;&#55;&#52;%7&#52;in&#103;%73&#46;c&#111;&#46;&#117;k'>info&#64;rmglett&#105;&#110;gs&#46;&#99;o&#46;uk</a></p>
		                    
					</div>-->
					
					<div class="con-slide" data-slide-id="facilities">
						<h3 class="bluefontgotham fontxl">Facilities</h3>
		                    <p class="greytxt fontmd">In your room you will have a Double bed, Wardrobe, Fitted Carpets, Curtains/Blinds, TV Point and Sink.</p>
		                    <p  class="fontmd">All sites offer the following:                      
		                  </p>
		                  <ul class="squareul fontmd">
		                            <li>Night Security</li>
		                            <li>Secure Fob Entry</li>
		                            <li>CCTV</li>
		                            <li>24hr Dedicated Staff</li>
		                            <li>Central Locations</li>
		                            <li>Free Parking</li>
									<li>Direct line to taxi service</li>
		                            <li>Fitted Kitchen: Hob Oven, Fridge, Freezer, Toaster, Kettle, Microwave</li>
		                            <li>On-site Laundry Facilities</li>
		                            <li>Communal Relaxation Areas</li>
		                            <li>TV point in every room</li>
		                            <li>2 acre communal garden</li>
									<li>Vending machines</li>
		                            
		                        </ul>
					</div>
					
					<div class="con-slide" data-slide-id="rates">
						<h3 class="bluefontgotham fontxl">Rates</h3>
							<p class="greytxt fontmd">The rent includes all bills and internet access. Please find below breakdown of the tariff:</p>
							<p class="fontmd">
								At RMG Lettings we pride ourselves on flexibility and have a minimum stay of 28 days.
								</p>
								<ul class="squareul fontmd">
									<li>Long term (12 weeks +) : &pound;75 per week</li>
									<li>Short term (less then 12 weeks) : &pound;80 per week</li>
								</ul>
							
							<p>
								Please find below details of the contracts which we offer:</p>
			                        <ul class="squareul">
				                        <li>20 weeks (01/09/2013 - 31/01/2013)</li>
				                         <li>48 weeks (31/08/2013 - 01/08/2014)</li>
				                         <li>52 weeks (31/08/2013 - 30/08/2014)</li>
				                    </ul>
			                        
		                   
					</div>
					
					<div class="con-slide" data-slide-id="details">
						<h3 class="bluefontgotham fontxl">Details</h3>
						<div class="padleft15 fontmd">
							<span class="lbluefont fontlg">Main House</span>
							<p>
								There are 3 floors in this building which are all provided with
								a communal kitchen. The floors are made up of separate wings for
								male and female residents, each containing 20 rooms. Also located
								on each floor is laundry access, and a communal bathroom that
								contains 2 showers, 1 bath and 4 toilets, there is a disabled
								toilet located on the ground floor. All bedrooms come with a
								double bed, wardrobe, desk, sink and gas central heating.
							</p>
							<p>
								The rooms on the ground floor are particularly sought after due
								to their location and view overlooking the 2 acre garden. The
								garden comes complete with football nets, volleyball nets, outdoor
								seating area and BBQ equipment.
							</p>
							 <p>
								There is a small gym room with rower, running machine, power
								plate, cross trainer and bike and 2 sets of free weights and
								benches.
							</p>
							<span class="lbluefont fontlg">Annex<br/></span>
							<p>
								The Annex is located within the grounds area and there are 4
								floors which each benefit from large kitchen and lounge areas.
								There are 20 rooms per floor and 2 large bathrooms per floor with
								2 showers, 1 bath and 2 toilets. The floors have single sex
								option. This building is more appealing to a student who prefers a
								quieter lifestyle.
							</p>
							<p>
								"We have the option of both single sex and mixed sex floors in each building." 
							</p>
						</div>
					</div>	
					
					<div class="con-slide fontmd" data-slide-id="amenities">
						<h3 class="bluefontgotham fontxl">Amenities</h3>
							<ul class="squareul">
								<li>Tesco's - 0.8miles</li>
								<li>Shell Garage - 500ft</li>
								<li>Subway - 0.7 miles</li>
								<li>Asda - 1.5 miles</li>
							</ul>
							<span class="lbluefont fontlg padleft15">Campuses</span>
							<ul class="squareul">
								<li>Manchester Metropolitan Hollings Campus - 2 miles</li>
								<li>Manchester Metropolitan All Saints Campus - 1.9 miles</li>
								<li>University of Manchester Fallowfield Campus - 1.8 miles</li>
								<li>University of Manchester Oxford Road Campus - 2 miles</li>
								<li>Excel College - 2.6 miles </li>
								<li>Berlitz College - 2.5 miles</li>
								<li>The International Society - 1.8 miles</li>
								<li>Manchester Academy Of English - 1.3 miles</li>
								<li>EF International Language Schools - 1.4 miles</li>
								<li>Italian Language Solutions - 0.6 miles</li>
							</ul>
					</div>
					
					<div class="con-slide" data-slide-id="location">
						<h3 class="bluefontgotham fontxl">Location</h3>
							<div class="fontmd">
								<div class="greytxt">Click on the map to see where this accommodation is located.<br/><br/></div>
									<div class="container">
										<div class="row">
											<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
												<a href="https://maps.google.co.uk/maps?q=Demesne+Road,+Manchester+M16+8PH&hl=en&oe=utf-8&client=firefox-a&hnear=Demesne+Rd,+Manchester+M16,+United+Kingdom&t=m&z=16&iwloc=A"
													target="_blank">
													<img class="img-responsive" src="../images/manchester_map.jpg" />
												</a>
											</div>
											<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
												<br/>
												Demesne Road,<br /> 
												Manchester<br />
												M16 8PH
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<br/>
											"The bus that travels between Alexandra Park and Piccadilly station is the number 85 Stagecoach bus and passes all universities." Or "the 101 From princess park way" -
								 For bus time tables please follow the link below <a href="http://www.tfgm.com/buses/Pages/default.aspx">http://www.tfgm.com/buses/Pages/default.aspx </a>
											</div>
										</div>
									</div>
							</div>
					</div>
					
					<div class="con-slide" data-slide-id="what_to_bring">
						<h3 class="bluefontgotham fontxl">What to Bring</h3>
						<div class="fontmd">
							<p>As you are living away from home, you will need to bring the following items with you:</p>
			
							<ul class="squareul">
								<li>Clothes</li>
								<li>Bedding (including pillows)</li>
								<li>Towels</li>
								<li>Cutlery and crockery</li>
								<li>Pots, pans and cooking utensils</li>
								<li>Tea towels</li>
								<li>Coat hangers</li>
								<li>Personal computer</li>
								<li>Lamp suitable for task lighting</li>
								<li>Radio/TV (if required)</li>
								<li>Iron and Ironing Board</li>
								<li>Stationery</li>
							</ul>
							<p>
								We can provide a bedding pack for international students who are unable to bring this with them. Please ensure this is requested before you arrive.<br/>
								
								Not all rooms are equipped with a desk chair. If you wish to have one in your room, please check if you need to bring one with you.<br/>
								
								If you are bringing a TV with you, you will need to apply for a TV licence which you can do online. (<a href="http://www.tvlicensing.co.uk">http://www.tvlicensing.co.uk</a>)<br/>
							</p>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	</div>
	
	
	
	<!--  Make an enquire Page Start -->
	<div class="section" id="section2">
		<div class="intro">
			<?php require_once 'contact.php';?>
			<div class="whitebg" id="contact">
				<div class="container paddlrb15">
					<div class="bluefont fontbg">Make an Enquiry</div>
					<? if($_REQUEST['a'] == "s" && $save_result !== true){?>
	                <p class="text-danger fontmd"><?=$save_result?></p>
	                <? }?>
	                
	                <? if($_REQUEST['a'] == "s" && $save_result === true){?>
		                <p class="text-success fontmd">Thank you for your enquiry. Someone will be in touch with you soon.</p>
	                <? }else{ ?>
					<p class="text-primary fontmd">To book a room or for any further enquiries please fill complete the enquiry form below. (<span class="text-danger">*</span>) required fields.
					</p>
					<div class="row">
					<form role="form" id="form1" name="form1" method="post">
	                    <input type="hidden" name="a" value="s" />
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 fontsm">
							  <div class="form-group">
							    <label for="title">Title</label>
							    <select class="form-control" name="title" id="title">
	    	                        	<option value="" selected="selected">-- please select --</option>
	                                    <option value="Ms" <? if($_REQUEST['title'] == "Ms"){?>selected="selected"<? }?>>Ms</option>
	                                    <option value="Miss" <? if($_REQUEST['title'] == "Miss"){?>selected="selected"<? }?>>Miss</option>
	                                    <option value="Mrs" <? if($_REQUEST['title'] == "Mrs"){?>selected="selected"<? }?>>Mrs</option>
	                                    <option value="Mr" <? if($_REQUEST['title'] == "Mr"){?>selected="selected"<? }?>>Mr</option>
								</select>
							  </div>
							<div class="form-group">
							    <label for="first_name">First Name(s)&nbsp;<span class="text-danger">*</span></label>
							    <input type="text"  class="form-control" name="first_name" id="first_name" value="<?=$_REQUEST['first_name']?>" placeholder="First Name"/>
							  </div>
							  <div class="form-group">
							    <label for="last_name">Surname&nbsp;<span class="text-danger">*</span></label>
							    <input type="text"  class="form-control" name="last_name" id="last_name" value="<?=$_REQUEST['last_name']?>" placeholder="Surname"/>
							  </div>
							  <div class="form-group">
							    <label for="email">Email&nbsp;<span class="text-danger">*</span></label>
							    <input type="email"  class="form-control" name="email" id="email" value="<?=$_REQUEST['email']?>" placeholder="Email"/>
							  </div>
							  <div class="form-group">
							    <label for="tel">Telephone no.&nbsp;<span class="text-danger">*</span></label>
							    <input type="text"  class="form-control" name="tel" id="tel" value="<?=$_REQUEST['tel']?>" placeholder="Telephone"/>
							  </div>
							  <div class="form-group">
							    <label for="development">Which development are you enquiring about?&nbsp;<span class="text-danger">*</span></label>
							     <select class="form-control" name="development" id="development">
	    	                        	<option value="" selected="selected">-- please select --</option>
	                                    <option value="Manchester" <? if($_REQUEST['development'] == "Manchester"){?>selected="selected"<? }?>>Manchester</option>
	                                    <option value="None" <? if($_REQUEST['development'] == "None"){?>selected="selected"<? }?>>None</option>
								</select>
							  </div>
							  <div class="form-group">
							    <label for="address">Current address&nbsp;<span class="text-danger">*</span></label>
							     <textarea class="form-control" name="address" id="address" rows="3"></textarea>
							  </div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 fontsm">
							<div class="form-group">
							    <label for="move_in_date">Move-in date&nbsp;<span class="text-danger">*</span></label>
							    <input type="text"  class="form-control date" name="move_in_date" id="move_in_date" value="<?=$_REQUEST['move_in_date']?>" placeholder="Move-in date"/>
							  </div><div class="form-group">
							    <label for="move_out_date">Move-out date&nbsp;<span class="text-danger">*</span></label>
							    <input type="text"  class="form-control date" name="move_out_date" id="move_out_date" value="<?=$_REQUEST['first_name']?>" placeholder="Move-out date"/>
							  </div>
							  <div class="form-group">
							    <label for="first_name">Preferred method of contact&nbsp;<span class="text-danger">*</span></label>
							   	<select class="form-control" name="method" id="method">
	    	                        	<option value="" selected="selected">-- please select --</option>
	                                    <option value="Telephone" <? if($_REQUEST['method'] == "Telephone"){?>selected="selected"<? }?>>Telephone</option>
	                                    <option value="Email" <? if($_REQUEST['method'] == "Email"){?>selected="selected"<? }?>>Email</option>
	                            </select>
							  </div>
							  <div class="form-group">
							    <label for="comments">Comments / questions</label>
							    <textarea class="form-control" name="comments" id="comments" rows="7"><?=$_REQUEST['comments']?></textarea>
							  </div>
							  <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-superlg-3 pull-right btn-custom btn-lg fontmd" onclick="send_form();return false;" title="Click to send">Enquire Now</div>
						</div>
					</div>	
					<?php }?>	
				</div>
			</div>
		</div>
	</div>	
	<!--  Make an enquire Page ends -->	
		
	<!--  Student Life Starts -->
	<div class="section" id="section3">
		<div class="body-conslider">
		   <div id="student_slider" class="conSlider">
				<div class="con-menu">
					<ul>
						<li><a href="#montyhall"><span class="bluefontgotham fontmd">Monty Halls</span></a></li>
						<li><a href="#monthouse"><span class="bluefontgotham fontmd">Montgomery House</span></a></li>
						<li><a href="#arndale"><span class="bluefontgotham fontmd">Manchester Arndale</span></a></li>
						<li><a href="#operahouse"><span class="bluefontgotham fontmd">Opera House</span></a></li>
					</ul>
				</div>
				<div class="con-container fontmd">
					<div class="con-slides"> 						
						<div class="con-slide" data-slide-id="montyhall">
							<h3 class="bluefontgotham fontxl">Monty Halls</h3>
							<div class="row">
								<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
									<img src="images/montyhall_1.jpg" class="img-responsive" />
								</div>
								<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
									<img src="images/montyhall_2.jpg" class="img-responsive" />
								</div>
								<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
									<img src="images/montyhall_3.jpg" class="img-responsive" />
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<p>If you are looking for student accommodation in Manchester which is great value affordable housing with all inclusive bills and 
									internet costs then a room at Monty Halls could be your new home. Whether you are at University, College or studying in the city centre we are only a short bus 
									ride away or walk located in the idyllic surroundings of Whalley Range. </p>
								</div>
							</div>
						</div>
						<div class="con-slide" data-slide-id="monthouse">
							<h3 class="bluefontgotham fontxl">Montgomery House</h3>
							<p>There is a large cascade of fun and interesting this to do in and around Montgomery Halls; there is a range of different bars, restaurants, museums, clubs pubs,
							 shops and much more. Whether you're into music and socialising or more energetic pursuits like biking and rock climbing there is something for you which is 
							 probably what make it the largest student community in the UK and Europe.</p>
							
						</div>
						<div class="con-slide" data-slide-id="arndale">
							<h3 class="bluefont fontxl">Manchester Arndale,  Market st.</h3>
							<div class="row">
								<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
									<img src="images/arndale.jpg" class="img-responsive" />
								</div>
								<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
									<p>Manchester is a great shopping experience, including the Arndale Centre, The Trafford Centre, Affleck's and Market Street shopping areas plus Selfridges, 
										Harvey Nicholls and House of Fraser.  As well as several smaller shops in areas such as Chorlton and Withington within a short distance of our student accommodation. 
										For restaurants, bars and Cinemas why not head to the Printworks where there is a really vibrant nightlife. 
										Ranging from a night at the cinema to drinks with friends. Other areas that are a must visit for a night out include Canal Street, 
										The Northern Quarter, Deansgate and Deansgate Locks and many student bars on and around Oxford road. If it's music you're into then there are many bars and clubs, 
										including many which off live music ranging from smaller venues such as the Academy and O2 Apollo to  MEN Arena, the palace theatre  and the opera house for live 
										shows visit the Box office. If its food your after you could also try Manchester's China Town or Didsbury town centre which is just a short bus trip away.</p>
								</div>
							</div>
						</div>
						<div class="con-slide" data-slide-id="operahouse">
							<h3 class="bluefontgotham fontxl">Opera House</h3>
							<div class="row">
								<div class="col-xs-5 col-sm-5col-md-5 col-lg-5">
									<img src="images/opera_house.jpg" class="img-responsive img-center" /><br/>
								</div>
								<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
									<p>If you are looking for some culture there are several world class museums, art galleries and several world famous sports venues including Premier League
									 giants Manchester United and Manchester City football clubs. You can also visit Old Trafford Cricket Ground, one of the UK's major international cricket venues or 
									 there's the National Football Museum, Science and Industry, The Imperial War Museum North and The Manchester Museum, plus both the Whitworth Art Gallery and the 
									 Manchester Art Gallery.</p>
									<p>How about something to keep active why not try one of our many attractions such as Manchester aquatics centre, The dance house, Sale water parks, Manchester climbing 
									centre, Airkix where you can really fly; the Chill factor if ski, snowboard, or air boarding take your fancy. Or maybe you would prefer a slower pace try one of our 
									many spa hotel or maybe something in between they one of our indoor climbing centres or a Vertical chill ice wall.</p>						
									<p>What ever it is you're looking for you can find in here making Manchester the perfect city to get the most out of student life.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
</div>
	<!--  Student Life ends -->
	
	<!-- Gallery Start -->
	<?php 
		$pics[6775] = 'Exterior Rear Shot';
		$pics[1447] = 'Annex Bathrooms';
		$pics[1450] = 'Annex Lounge';
		$pics[1453] = 'Annex Kitchen1';
		$pics[1454] = 'Annex Kitchen2';
		$pics[1459] = 'Annex Bedroom';
		$pics[1466] = 'House Bedroom';
		$pics[1467] = 'House Common Room';
		$pics[1474] = 'Common Room Leading to Gardens';
		$pics[1488] = 'BBQ area and Football nets';
		$pics[1494] = 'House rear and football nets';
		$pics[1500] = 'House Gym';
		$pics[1532] = 'Alexandra Park';
	?>
	<div class="section" id="section4">
		<div class="gallery_slider">		
			<div id="gallery_slider" class="conSlider">
				<div class="con-menu">
					<ul>
					<?php  foreach($pics as $key =>$val) {?>
						<li><a href="#<?= $val;?>"><span class="bluefontgotham fontmd"><?= $val;?></span></a></li>
					<?php  }?>
					</ul>
				</div>
				<div class="con-container fontmd">
					<div class="con-slides"> 		
						<?php  foreach($pics as $img_id =>$title) {?>				
						<div class="con-slide" data-slide-id="<?= $title;?>">
							<!-- <h3 class="bluefont fontxl"><?= $title;?></h3>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> -->
									<img src="images/Mont_<?=$img_id;?>.jpg" class="img-responsive img-center" />
								<!-- </div>
							</div> -->
						</div>	
						<?php  }?>					
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Gallery End -->
	<div class="section" id="section5">
		<div class="video-container">
		    <iframe src="http://www.youtube.com/embed/dPmIXJsMZlw" id="montgomery_house" allowfullscreen="" frameborder="0">
		    </iframe>
		</div>
	</div>
	
	</div>
</body>
</html>