<?

class data {

	function date_to_ts($date_str, $input_format="Ymdhi"){
		
		if($input_format == "Ymdhi"){

			$y = substr($date_str,0,4);
			$m = substr($date_str,4,2);
			$d = substr($date_str,6,2);
			$hour = substr($date_str,8,2);
			$min = substr($date_str,10,2);
			$sec = 0;

			$date = new DateTime($y."-".$m."-".$d." ".$hour.":".$min);
			$ts = $date->format("U");
			
			return $ts;
		}
		
		return false;
	}
	
	function ymd_to_date($ymd="", $format="d/m/Y"){
	
		if($ymd != ""){
	
			$d = substr($ymd, 6, 2);
			$m = substr($ymd, 4, 2);
			$y = substr($ymd, 0, 4);
		
			// Added to deal with post 2038 dates
			try{
				$date = new DateTime($y.'-'.$m.'-'.$d);
				return $date->format($format);
			}catch(Exception $e){
				return false;
			}
		
		}
		
		//$ts = mktime(1,1,1,$m,$d,$y);
		//return date($format, $ts);
	}
	
	function date_to_ymd($date, $format="d/m/Y"){
	
		if($format == "d/m/Y"){
			$date_parts = explode("/", $date);
			return $date_parts[2].$date_parts[1].$date_parts[0];
		}else{
			return false;
		}
	}
	
	function ymdhi_to_date($ymdhi, $output_format="d/m/Y"){
	
		if($ymdhi != ""){
		
			$i = substr($ymdhi, 10, 2);
			$h = substr($ymdhi, 8, 2);
			$d = substr($ymdhi, 6, 2);
			$m = substr($ymdhi, 4, 2);
			$y = substr($ymdhi, 0, 4);

			// Added to deal with post 2038 dates
			$date = new DateTime($y.'-'.$m.'-'.$d.' '.$h.':'.$i);
			return $date->format($output_format);
		}
		return "";
	}
	
	function ymdhi_to_ts($ymdhi){
	
		$i = substr($ymdhi, 10, 2);
		$h = substr($ymdhi, 8, 2);
		$d = substr($ymdhi, 6, 2);
		$m = substr($ymdhi, 4, 2);
		$y = substr($ymdhi, 0, 4);
		
		$ts = mktime($h,$i,1,$m,$d,$y);
		
		return $ts;
	}
	
	
	
	/**
	* function date_adjustment () 
	*
	* @param $input_date
	* @param $amount
	* @param $type  = year,month,day,hour,minute,second
	* @param $dir = +,-
	* @param $input_format
	* @param $output_format
	* @return date in $output_format
	*/
	function date_adjustment($input_date="", $amount, $type, $dir, $input_format="d/m/Y", $output_format="Ymd"){
		
		if($input_date != "" && $amount != "" && $type != "" && $dir != ""){
			
			if($input_format == "d/m/Y"){
				
				$date_parts = explode("/", $input_date);
				$date = new DateTime($date_parts[2]."-".$date_parts[1]."-".$date_parts[0]);
			}
			elseif($input_format == "Ymd"){
				
				$d = substr($input_date, 6, 2);
				$m = substr($input_date, 4, 2);
				$y = substr($input_date, 0, 4);
				$date = new DateTime($y."-".$m."-".$d);
			}
			elseif($input_format == "YmdHi"){
				
				$i = substr($input_date, 10, 2);
				$h = substr($input_date, 8, 2);
				$d = substr($input_date, 6, 2);
				$m = substr($input_date, 4, 2);
				$y = substr($input_date, 0, 4);
				$date = new DateTime($y."-".$m."-".$d." ".$h.":".$i); 
			}
			
			$date->modify($dir.$amount.' '.$type);
			return $date->format($output_format);
		}
	}
	
	
	function daylight_saving_diff($from_ts, $to_ts){
	
		$from_result = date("I",$from_ts);
		$to_result = date("I",$to_ts);
		
		if($from_result == "1" && $to_result == "0"){
			return 3600;
		}
		elseif($from_result == "0" && $to_result == "1"){
			return -3600;
		}
		
		return 0;
	}
	
	function is_day_of_march_dst_change($year, $month, $day){

		// Last Sunday in Mar?
		if($month == 3){
			for($d=0;$d<=7;$d++){
				$ts = mktime(12,0,0,3,(31-$d),$year);
				if(date("N",$ts) == 7){
					if(31-$d == $day){return true;}
					break;
				}
			}
		}
		
		return false;
	}
	
	function is_day_of_oct_dst_change($year, $month, $day){
	
		// Last Sunday in Oct?
		if($month == 10){
			for($d=0;$d<=7;$d++){
				$ts = mktime(12,0,0,10,(31-$d),$year);
				if(date("N",$ts) == 7){
					if(31-$d == $day){return true;}
					break;
				}
			}
		}
		
		return false;
	}
	
	function auto_null($str){
		if($str == ""){
			return "NULL";
		}
		else{
			return $str;
		}
	}
	
	function convert_file_size_to_name($bytes=""){ // Size should be supplied in kb

		if($bytes == ""){return "";}

		if ($bytes < 1024) return $bytes.' B';
		elseif ($bytes < 1048576) return round($bytes / 1024, 2).' KB';
		elseif ($bytes < 1073741824) return round($bytes / 1048576, 2).' MB';
		elseif ($bytes < 1099511627776) return round($bytes / 1073741824, 2).' GB';
		else return round($bytes / 1099511627776, 2).' TB';
	}
	
	
	function clean_xml_data($str){
	
		$str = str_replace("&", "&amp;", $str);
		$str = str_replace(">", "&gt;", $str);
		$str = str_replace("<", "&lt;", $str);
		$str = str_replace("\"", "&quot;", $str);
		$str = str_replace("'", "&#39;", $str);
		return $str;
	}
	
	
	function strip_illegal_filename_chars($filename){

		$bad = array_merge( array_map('chr', range(0,31)), array("<", ">", ":", '"', "/", "\\", "|", "?", "*"));
		$result = str_replace($bad, "", $filename);
		return $result;
	}
	
	
	// Primarily used to with ini_get for file uploads.
	function return_bytes($val) {

		$val = trim($val);
		$last = strtolower($val[strlen($val)-1]);
		switch($last) {
			// The 'G' modifier is available since PHP 5.1.0
			case 'g':
				$val *= 1024;
			case 'm':
				$val *= 1024;
			case 'k':
				$val *= 1024;
		}
	
		return $val;
	}
	
	
	function get_file_icon($file_name, $size_cat=""){
		
		$file_ext_parts = explode(".", $file_name);
		$ext = strtolower($file_ext_parts[ (count($file_ext_parts)-1) ]);
		
		if( $ext == "doc" || $ext == "docx" ){
			if($size_cat == "medium"){
				return "/images/file_type_icons/medium/doc.png";
			}
			else{
				return "/images/file_type_icons/doc.png";
			}
		}
		elseif( $ext == "pdf" ){
			if($size_cat == "medium"){
				return "/images/file_type_icons/medium/pdf.png";
			}
			else{
				return "/images/file_type_icons/pdf.png";
			}
		}
		elseif( $ext == "jpeg" || $ext == "jpg" || $ext == "gif" || $ext == "png" ){
			if($size_cat == "medium"){
				return "/images/file_type_icons/medium/img.png";
			}
			else{
				return "/images/file_type_icons/jpeg.gif";
			}
		}
	}
	
	// expects num to start from zero, i.e. 0 == A, 1 == B, etc.
	function excel_index_to_col($num,$start=65,$end=90, $zero_indexed=true){
		
		if($zero_indexed === true){
			$num++;
		}
		
		$sig = ($num < 0);
		$num = abs($num);
		$str = "";
		$cache = ($end-$start);
		while($num != 0){
			$str = chr(($num%$cache)+$start-1).$str;
			$num = ($num-($num%$cache))/$cache;
		}
		if($sig){
			$str = "-".$str;
		}
		return $str;
	}
	
	
	function crypt($dataToEncrypt){
		
		$appKey = 'sdf15#13P0sdfsfsdff8asdc%/dfr_A!8792*dsszdm,fjvgerkvtb54w5dfgdfgsfs5';
		$td = mcrypt_module_open(MCRYPT_DES, '', MCRYPT_MODE_CBC, '');
		
		// Creates IV and gets key size
		$iv = mcrypt_create_iv( mcrypt_enc_get_iv_size($td) , MCRYPT_RAND);
		$ks = mcrypt_enc_get_key_size($td);
		
		// Creates key from application key
		$key = substr($appKey, 0, $ks);
		
		// Initialization
		mcrypt_generic_init($td, $key, $iv);
		
		// Crypt data
		$encrypted = mcrypt_generic($td, $dataToEncrypt);
		
		// Close
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);
		return array($encrypted, $iv);
	}
	
	
	function decrypt($encryptedData, $iv){
		
		$appKey = 'sdf15#13P0sdfsfsdff8asdc%/dfr_A!8792*dsszdm,fjvgerkvtb54w5dfgdfgsfs5';
		$td = mcrypt_module_open(MCRYPT_DES, '', MCRYPT_MODE_CBC, '');
		
		// Gets key size
		$ks = mcrypt_enc_get_key_size($td);
		
		// Creates key from application key
		$key = substr($appKey, 0, $ks);
		
		// Initialization
		mcrypt_generic_init($td, $key, $iv);
		
		// Decrypt data
		$decrypted = mdecrypt_generic($td, $encryptedData);
		
		// Close
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);
		
		return trim($decrypted);
	}
	
}

?>