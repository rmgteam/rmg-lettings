<?php
/*
 * This class is the base class 
 */
require_once($UTILS_SERVER_PATH."library/security.class.php");
require_once($UTILS_SERVER_PATH."library/data.class.php");
require_once($UTILS_SERVER_PATH."library/mysql.class.php");

class core {
	
	function __construct(){
	}
	
	function gen_unique_serial($table, $serial_field){
		
		$security = new security;
		$loop = "Y";
		
		while($loop == "Y"){
			
			$serial = $security->gen_serial(32);
			$sql = "
			SELECT count(*) 
			FROM ".$table."  
			WHERE 
			".$serial_field." = '".$serial."' 
			";
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] == 0){
				$loop = "N";
				return $serial;
			}
		}
	}
	
	// Gets db insert id
	function get_last_insert_id($table){
		
		$sql = "SELECT LAST_INSERT_ID() FROM ".$table;
		$result = @mysql_query($sql);
		$row = @mysql_fetch_row($result);
		return $row[0];		
	}
	



}
?>