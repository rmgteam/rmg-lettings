<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>RMG Lettings</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen" />
		<link href="../css/common.css" rel="stylesheet" type="text/css" media="screen" />
		
		<!--[if lte IE 7]>
		<link href="/css/lte_ie7.css" rel="stylesheet" type="text/css" media="screen" />
		<![endif]-->
		<!--[if lte IE 8]>
		<link href="/css/lte_ie8.css" rel="stylesheet" type="text/css" media="screen" />
		<![endif]-->
		<!--[if lte IE 9]>
		<link href="/css/lte_ie9.css" rel="stylesheet" type="text/css" media="screen" />
		<![endif]-->
		<script type="text/javascript" src="../js/jquery.min.js"></script>
		<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	</head>
	<body>
		<?
		require_once("../utils.php");
		require_once("../includes/header.php");
		?>
        <div class="container">
            <div class="row text-center">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-md-offset-3">
                	<img src="../images/logo.png" class="img-responsive" style="margin-top:60px;">
                </div>
              </div>
            <div class="row text-center">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
				<div class="bluefont fontxxl">Doh!</div>
                <p>&nbsp;</p>
                <p class="greyfont fontxl">Your application link has expired. If you wish to receive another online lettings application link, please request one by sending an email to <?=$UTILS_CONTACT_EMAIL_TAG?></p>
                </div>
            </div>
        </div>
    
	

</body>
</html>