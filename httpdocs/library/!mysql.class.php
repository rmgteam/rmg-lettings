<?php


class mysql{
	
	var $conn;			//Variable to hold connection

		
	/* 
	 * Set connection to global connection on creation of object
	 */
	function mysql(){
		
		global $conn;
		
		if($conn != ""){
			$this->conn = $conn;	
		}
	}
	
	/* 
	 * Create new Connection
	 * 
	 * @param string $host		Host Name
	 * @param string $user		Username
	 * @param string $password	Password
	 */
	function connect($host, $user, $password){
		$this->conn = mysql_connect($host,$user,$password);
		mysql_set_charset("utf8");
		return $this->conn;
	}
	
	/* 
	 * Select Database
	 * 
	 * @param string $db_name		Database Name
	 */
	function select_db($db_name){
		mysql_select_db($db_name, $this->conn);
	}
	
	/* 
	 * Submit Query
	 * 
	 * @param string $query		SQL Statement
	 * @param string $type		Friendly name to find in logs upon failure
	 */
	function query($query, $type='SQL Query'){
		
		$has_error = false;
		
		$result = mysql_query($query, $this->conn);
		
		if($has_error == true){
			return false;
		}else{
			return $result;
		}
	}
	
	/* 
	 * Insert
	 * 
	 * @param string $query		SQL Statement
	 * @param string $type		Friendly name to find in logs upon failure
	 */
	function insert($query, $type_of='SQL Query'){
		
		$has_error = false;
		
		$result = mysql_query($query, $this->conn);
		
		if($has_error === true){
			return false;
		}else{
			
			//When we move to mySQLi use function not this statement 
			$last_id = '';
			
			$sql = "SELECT LAST_INSERT_ID()";
			$result = $this->query($sql, 'Get Last ID');
			$num_rows = $this->num_rows($result);
			if($num_rows > 0){
				while($row = $this->fetch_array($result)){
					$last_id = $row[0];
				}
			}

			return $last_id;
		}
	}
	
	/* 
	 * Retrieve Data
	 * 
	 * @param string $result	MySQL resource
	 * @param string $type_of	Type Description
	 * @param string $type		Type of fetch (MYSQL_BOTH as default)
	 */
	function fetch_array($result, $type='MYSQL_BOTH'){
		if (is_bool($result)) {
        	return false;
		}else{
			if ($type == 'MYSQL_ASSOC') $row = mysql_fetch_array($result, MYSQL_ASSOC);
			if ($type == 'MYSQL_NUM') $row = mysql_fetch_array($result, MYSQL_NUM);
			if ($type == 'MYSQL_BOTH') $row = mysql_fetch_array($result, MYSQL_BOTH); 
	      
	      	if (!$row) return false;
			
			return $row;
		}
	}
	
	/* 
	 * Retrieve Number of Rows
	 * 
	 * @param string $result	MySQL resource
	 * @param string $type_of	Type Description
	 */
	function num_rows($result, $type_of='MySQL Number of Rows'){
		if (is_bool($result)) {
        	return false;
		}else{
			return mysql_num_rows($result);
		}
	}
	
	/* 
	 * Seek to row number
	 * 
	 * @param string $result	MySQL resource
	 * @param string $row		Row number (0 as default)
	 * @param string $type_of	Type Description
	 */
	function data_seek($result, $row=0, $type_of='MySQL Data Seek'){
		if (is_bool($result)) {
        	return false;
        }else{
			return mysql_data_seek($result, $row);
        }
	}
	
}

?>