<?
require_once("../utils.php");
require_once($UTILS_SERVER_PATH."library/country.class.php");
require_once($UTILS_SERVER_PATH."library/nationality.class.php");
require_once($UTILS_SERVER_PATH."library/applications.class.php");
$country = new country;
$nationality = new nationality;
$applications = new applications;

// Checks to see if user has already submitted their applications
if( $_REQUEST['code'] == "" || $applications->already_submitted($_REQUEST['code']) === true ){
	header("Location: success.php?sub=Y");
	exit;	
}

// Checks to see link has expired
if( $applications->has_link_expired($_REQUEST['t']) === true ){
	header("Location: expired.php");
	exit;	
}


// Submit application 
if( $_REQUEST['a'] == "s" ){
	
	$save_success = true;
	
	$save_result = $applications->check_fields($_REQUEST); 
	if($save_result['save_result'] != "success"){
		$save_success = $save_result['save_msg'];
	}
	else{
		$save_result = $applications->save($_REQUEST); 
		if($save_result !== true){
			$save_success = $save_result; 
		}
		else{
			header("Location: success.php");
			exit;	
		}
	}
}

// Set page defaults
if( $_REQUEST['a'] != "s" ){
	
	$_REQUEST['kin_address_country_input'] = 15;	
	$_REQUEST['home_nation_input'] = 28;
	$_REQUEST['home_address_country_input'] = 15;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>RMG Lettings</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/application.css" rel="stylesheet" type="text/css" media="screen" />

<!--[if lte IE 7]>
<link href="/css/lte_ie7.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->
<!--[if lte IE 8]>
<link href="/css/lte_ie8.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->
<!--[if lte IE 9]>
<link href="/css/lte_ie9.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->

<script type="text/javascript" src="../js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="../js/cufon-yui.js"></script>
<script type="text/javascript" src="../js/VAG_rounded_700.font.js"></script>
<script type="text/javascript">
	Cufon.replace('h1'); // Works without a selector engine
	Cufon.replace('h2');
	Cufon.replace('h3');
	Cufon.replace('h4');
	Cufon.replace('h5');
	//Cufon.replace('#sub1'); // Requires a selector engine for IE 6-7, see above
</script>
<script type="text/javascript">
$(document).ready(function(){
	
    $("#home_address_country_input").change(
    	function() {
        	
			if($(this).val() == "1"){
				$("#home_address_postcode_label").html("Zip code");
			}
			else{
				$("#home_address_postcode_label").html("Postcode");
			}
			
			if($(this).val() == "15"){
				$("#home_address_postcode_label").html("Postcode");
			}
			else{
				$("#home_address_postcode_label").html("Postcode&nbsp;*");
			}
    	}
    );

    $("#kin_address_country_input").change(
       	function() {
            	
   			if($(this).val() == "1"){
   				$("#kin_address_postcode_label").html("Zip code");
   			}
   			else{
   				$("#kin_address_postcode_label").html("Postcode");
   			}
   			
   			if($(this).val() == "15"){
   				$("#kin_address_postcode_label").html("Postcode");
   			}
   			else{
   				$("#kin_address_postcode_label").html("Postcode&nbsp;*");
   			}
       	}
    );
	
	$("#same_address").change(function(){
		toggle_kin_address();
	});
	
	toggle_kin_address();
   
});

function toggle_kin_address(){
	
	if( $("#same_address").is(':checked') ){
		$(".kin_address_block").hide();
	}
	else{
		$(".kin_address_block").show();
	}
}

function show_error(field_names, message){
	
	if( $.isArray(field_names) ){
		for(r=0;r<field_names.length;r++){
			$('#'+field_names[r]).addClass('text-info');
		}
	}
	
	$('#error_msg p').html(message);
	$('#error_msg').show();
}
</script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
</head>
<body>

    <div class="container">
        <div class="span12">
			
            <img src="../images/logo.png" style="margin-top:30px;">
            
            <h1>Lettings Online Application Form</h1>
            
            <div id="error_msg" class="msg_fail" style="display:none;">
                <p class="text-error"><?=$save_success?></p>
            </div>
            
            <p>Please complete the form below and click the <strong><em>Submit</em></strong> button at the bottom of this page. This will send your details to our lettings team who will process your application. Required fields are marked with (*)</p>
            
            <form class="form-inline form-horizontal" method="post">

                <input value="s" name="a" id="a" type="hidden">
                <input value="<?=$_REQUEST['code']?>" name="code" id="code" type="hidden">
                
                <h3>Your Details</h3>
                
                <!-- Applicant Names -->
                <div class="control-group">
                    <label class="control-label text-left" for="first_name_input" id="first_name_label">First name&nbsp;*</label>
                    <div class="controls">
                        <input type="text" id="first_name_input" name="first_name_input" class="input-xlarge" value="<?=$_REQUEST['first_name_input']?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label text-left" for="middle_name_input" id="middle_name_label">Middle name</label>
                    <div class="controls">
                        <input type="text" id="middle_name_input" name="middle_name_input" class="input-xlarge" value="<?=$_REQUEST['middle_name_input']?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label text-left" for="last_name_input" id="last_name_label">Last name&nbsp;*</label>
                    <div class="controls">
                        <input type="text" id="last_name_input" name="last_name_input" class="input-xlarge" value="<?=$_REQUEST['last_name_input']?>">
                    </div>
                </div>
                    
                <!-- Applicants address -->
                <div class="control-group">
                    <label class="control-label text-left" for="home_address_1_input" id="home_address_1_label">Street address 1&nbsp;*</label>
                    <div class="controls">
                        <input type="text" id="home_address_1_input" name="home_address_1_input" class="input-xxlarge" value="<?=$_REQUEST['home_address_1_input']?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label text-left" for="home_address_2_input" id="home_address_2_label">Street address 2</label>
                    <div class="controls">
                        <input type="text" id="home_address_2_input" name="home_address_2_input" class="input-xxlarge" value="<?=$_REQUEST['home_address_2_input']?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label text-left" for="home_address_3_input" id="home_address_3_label">Street address 3</label>
                    <div class="controls">
                        <input type="text" id="home_address_3_input" name="home_address_3_input" class="input-xxlarge" value="<?=$_REQUEST['home_address_3_input']?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label text-left" for="home_address_town_input" id="home_address_town_label">Town/City&nbsp;*</label>
                    <div class="controls">
                        <input type="text" id="home_address_town_input" name="home_address_town_input" class="input-xlarge" value="<?=$_REQUEST['home_address_town_input']?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label text-left" for="home_address_county_input" id="home_address_county_label">County</label>
                    <div class="controls">
                        <input type="text" id="home_address_county_input" name="home_address_county_input" class="input-xlarge" value="<?=$_REQUEST['home_address_county_input']?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label text-left" for="home_address_postcode_input" id="home_address_postcode_label">Postcode&nbsp;*</label>
                    <div class="controls">
                        <input type="text" id="home_address_postcode_input" name="home_address_postcode_input" class="input-medium" value="<?=$_REQUEST['home_address_postcode_input']?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label text-left" for="home_address_country_input" id="home_address_country_label">Country&nbsp;*</label>
                    <div class="controls">
                        <select id="home_address_country_input" name="home_address_country_input">
                            <?=$country->gen_country_list_select($_REQUEST['home_address_country_input'])?>
                        </select>
                    </div>
                </div>
                        
                <!-- Applicants additional details -->
                <div class="control-group">
                   <label class="control-label text-left" for="home_tel_1_input" id="home_tel_1_label">Tel. no. 1&nbsp;*</label>
                    <div class="controls">
                        <input type="text" id="home_tel_1_input" name="home_tel_1_input" class="input-medium" value="<?=$_REQUEST['home_tel_1_input']?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label text-left" for="home_tel_2_input" id="home_tel_2_label">Tel. no. 2</label>
                    <div class="controls">
                        <input type="text" id="home_tel_2_input" name="home_tel_2_input" class="input-medium" value="<?=$_REQUEST['home_tel_2_input']?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label text-left" for="home_email_input" id="home_email_label">Email&nbsp;*</label>
                    <div class="controls">
                        <input type="text" id="home_email_input" name="home_email_input" class="input-xlarge" value="<?=$_REQUEST['home_email_input']?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label text-left" for="home_dob_input" id="home_dob_label">Date of birth&nbsp;*</label>
                    <div class="controls">
                        <input type="text" id="home_dob_input" name="home_dob_input" class="input-small" maxlength="10" value="<?=$_REQUEST['home_dob_input']?>"> (dd/mm/yyyy)
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label text-left" for="home_gender_input" id="home_gender_label">Gender&nbsp;*</label>
                    <div class="controls">
                        <select id="home_gender_input" name="home_gender_input">
                            <option selected="selected">-- select --</option>
                            <option value="M" <? if($_REQUEST['home_gender_input'] == "M"){?>selected="selected"<? }?>>Male</option>
                            <option value="F" <? if($_REQUEST['home_gender_input'] == "F"){?>selected="selected"<? }?>>Female</option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label text-left" for="home_nation_input" id="home_nation_label">Nationality&nbsp;*</label>
                    <div class="controls">
                        <select id="home_nation_input" name="home_nation_input">
                            <?=$nationality->gen_nat_list_select($_REQUEST['home_nation_input'])?>
                        </select>
                    </div>
                </div>
                
                
                <h3>Course details</h3> 
                <!-- Uni Info -->
                <div class="control-group">
                    <label class="control-label text-left" for="uni_name_input" id="uni_name_label">University/ College&nbsp;*</label>
                    <div class="controls">
                        <input type="text" id="uni_name_input" name="uni_name_input" class="input-xxlarge" value="<?=$_REQUEST['home_tel_1_input']?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label text-left" for="uni_course_input" id="uni_course_label">Course&nbsp;*</label>
                    <div class="controls">
                        <input type="text" id="uni_course_input" name="uni_course_input" class="input-xxlarge" value="<?=$_REQUEST['uni_course_input']?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label text-left" for="uni_student_id_input" id="uni_student_id_label">Student ID number</label>
                    <div class="controls">
                        <input type="text" id="uni_student_id_input" name="uni_student_id_input" class="input-large" value="<?=$_REQUEST['uni_student_id_input']?>">
                    </div>
                </div>    
                
                <!-- About your stay -->
                <div class="control-group">
                    <label class="control-label text-left" for="stay_start_date_input" id="stay_start_date_label">Move-in date&nbsp;*</label>
                    <div class="controls">
                        <input type="text" id="stay_start_date_input" name="stay_start_date_input" class="input-small" maxlength="10" value="<?=$_REQUEST['stay_start_date_input']?>"> (dd/mm/yyyy)
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label text-left" for="stay_end_date_input" id="stay_end_date_label">Move-out date&nbsp;*</label>
                    <div class="controls">
                        <input type="text" id="stay_end_date_input" name="stay_end_date_input" class="input-small" maxlength="10" value="<?=$_REQUEST['stay_end_date_input']?>"> (dd/mm/yyyy)
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label text-left" for="notes_input">Additional comments</label>
                    <div class="controls">
                        <p><small>All of our properties include floors that are specific to gender, or have disabled access rooms. If you require specific room allocation to a single sex floor or access to a disabled room then please specify this below.</small></p>
                        <textarea id="notes_input" name="notes_input" class="input-xxlarge" rows="5"><?=$_REQUEST['notes_input']?></textarea>
                    </div>
                </div>

    
                <h3>Next of Kin</h3>
                <p><small>Details of your nearest relative, or somebody that you would want us to contact in the case of an emergency.</small></p>
                    
                <!-- Next of Kin -->
                <div class="control-group">
                    <label class="control-label text-left" for="kin_first_name_input" id="kin_first_name_label">First name&nbsp;*</label>
                    <div class="controls">
                        <input type="text" id="kin_first_name_input" name="kin_first_name_input" class="input-xlarge" value="<?=$_REQUEST['kin_first_name_input']?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label text-left" for="kin_last_name_input" id="kin_last_name_label">Last name&nbsp;*</label>
                    <div class="controls">
                        <input type="text" id="kin_last_name_input" name="kin_last_name_input" class="input-xlarge" value="<?=$_REQUEST['kin_last_name_input']?>">
                    </div>
                </div>
                
                <div class="control-group" style="margin-bottom:0px;margin-top:30px;">
                    <div class="alert alert-info">
                    	<label class="checkbox" for="same_address">
                        	<input type="checkbox" id="same_address" name="same_address" value="Y" <? if($_REQUEST['same_address'] == "Y"){?>checked<? }?>>&nbsp;&nbsp;Next of Kin address is the same as the applicant above 
                    	</label>
                    </div>
                </div>
                
                <div class="control-group kin_address_block">
                    <label class="control-label text-left" for="kin_address_1_input" id="kin_address_1_label">Street address 1&nbsp;*</label>
                    <div class="controls">
                        <input type="text" id="kin_address_1_input" name="kin_address_1_input" class="input-xxlarge" value="<?=$_REQUEST['kin_address_1_input']?>">
                    </div>
                </div>
                <div class="control-group kin_address_block">
                    <label class="control-label text-left" for="kin_address_2_input" id="kin_address_2_label">Street address 2</label>
                    <div class="controls">
                        <input type="text" id="kin_address_2_input" name="kin_address_2_input" class="input-xxlarge" value="<?=$_REQUEST['kin_address_2_input']?>">
                    </div>
                </div>
                <div class="control-group kin_address_block">
                    <label class="control-label text-left" for="kin_address_3_input" id="kin_address_3_label">Street address 3</label>
                    <div class="controls">
                        <input type="text" id="kin_address_3_input" name="kin_address_3_input" class="input-xxlarge" value="<?=$_REQUEST['kin_address_3_input']?>">
                    </div>
                </div>
                <div class="control-group kin_address_block">
                    <label class="control-label text-left" for="kin_address_town_input" id="kin_address_town_label">Town/City&nbsp;*</label>
                    <div class="controls">
                        <input type="text" id="kin_address_town_input" name="kin_address_town_input" class="input-xlarge" value="<?=$_REQUEST['kin_address_town_input']?>">
                    </div>
                </div>
                <div class="control-group kin_address_block">
                    <label class="control-label text-left" for="kin_address_county_input" id="kin_address_county_label">County</label>
                    <div class="controls">
                        <input type="text" id="kin_address_county_input" name="kin_address_county_input" class="input-xlarge" value="<?=$_REQUEST['kin_address_county_input']?>">
                    </div>
                </div>
                <div class="control-group kin_address_block">
                    <label class="control-label text-left" for="kin_address_postcode_input" id="kin_address_postcode_label">Postcode&nbsp;*</label>
                    <div class="controls">
                        <input type="text" id="kin_address_postcode_input" name="kin_address_postcode_input" class="input-medium" value="<?=$_REQUEST['kin_address_postcode_input']?>">
                    </div>
                </div>
                <div class="control-group kin_address_block">
                    <label class="control-label text-left" for="kin_address_country_input" id="kin_address_country_label">Country&nbsp;*</label>
                    <div class="controls">
                        <select id="kin_address_country_input" name="kin_address_country_input">
                            <?=$country->gen_country_list_select($_REQUEST['kin_address_country_input'])?>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label text-left" for="kin_tel_1_input" id="kin_tel_1_label">Tel. no. 1</label>
                    <div class="controls">
                        <input type="text" id="kin_tel_1_input" name="kin_tel_1_input" class="input-medium" value="<?=$_REQUEST['kin_tel_1_input']?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label text-left" for="kin_tel_2_input" id="kin_tel_2_label">Tel. no. 2</label>
                    <div class="controls">
                        <input type="text" id="kin_tel_2_input" name="kin_tel_2_input" class="input-medium" value="<?=$_REQUEST['kin_tel_2_input']?>">
                    </div>
                </div>
                
                <div class="control-group">
                    <div class="form-actions">
                        <button type="submit" class="btn-large btn-info">Submit</button>
                    </div>
                </div>
            
            </form>
            
        </div>
    </div>
    
    <? if( $_REQUEST['a'] == "s" && $save_success !== true ){?>
	<script type="application/javascript">
	show_error(<?=json_encode($save_result['field_names'])?>, "<?=$save_result['save_msg']?>");
	</script>
    <? }?>

</body>
</html>