<?php
require_once($UTILS_SERVER_PATH."library/core.class.php");


class nationality extends core {
	
	
	function gen_nat_list(){
		
		$mysql = new mysql;
		$result = $mysql->query("
		SELECT * 
		FROM 
		nationality 
		WHERE 
		nationality_id <> 1 
		ORDER BY nationality_name ASC 
		");
		
		return $result;
	}
	
	
	function gen_nat_list_select($selected_val=""){
		
		$mysql = new mysql;
		$result = $this->gen_nat_list();
		
		if($selected_val == "1"){$selected = 'selected="selected"';}
		$list = '<option value="1" '.$selected.'>Unknown</option>';
		while( $row = $mysql->fetch_array($result) ){			
			
			$selected = "";
			if($selected_val == $row['nationality_id']){$selected = 'selected="selected"';}
			
			$list .= '<option value="'.$row['nationality_id'].'" '.$selected.'>'.$row['nationality_name'].'</option>';	
		}
		return $list;
	}
	
	
	// Helper function
	function get_nationality_name($nationality_id){
		
		$mysql = new mysql;
		$sql = "
		SELECT nationality_name  
		FROM nationality  
		WHERE 
		nationality_id = ".$nationality_id." 
		";
		$result = $mysql->query($sql);
		$row = $mysql->fetch_array($result);
		return $row[0];
	}

	
}


?>