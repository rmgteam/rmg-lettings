<?
require_once("../utils.php");
require_once($UTILS_SERVER_PATH."library/country.class.php");
require_once($UTILS_SERVER_PATH."library/nationality.class.php");
require_once($UTILS_SERVER_PATH."library/applications.class.php");
$country = new country;
$nationality = new nationality;
$applications = new applications;
Global $UTILS_TEL_LETTINGS_MAIN;
// Checks to see if user has already submitted their applications
  if( $_REQUEST['code'] == "" || $applications->already_submitted($_REQUEST['code']) === true ){
 	header("Location: success.php?sub=Y");
 	exit;	
 }  

// // Checks to see link has expired
  if( $applications->has_link_expired($_REQUEST['t']) === true ){
 	header("Location: expired.php");
 	exit;	
 }  

// Submit application 
if( $_REQUEST['a'] == "s" ){
	
	$result_array = array();
	$result_array['save_result'] = "success";

	$save_result = $applications->check_fields($_REQUEST); 
	if($save_result['save_result'] != "success"){
		$result_array = $save_result;
	}
	else{
		$save_result = $applications->save($_REQUEST); 
		if($save_result !== true){
			$result_array['save_msg'] = $save_result; 
			$result_array['save_result'] = "fail"; 
		}
	}
	
	echo json_encode($result_array);
	exit;
}

// Set page defaults
if( $_REQUEST['a'] != "s" ){
	
	$_REQUEST['kin_address_country_input'] = 15;	
	$_REQUEST['home_nation_input'] = 28;
	$_REQUEST['home_address_country_input'] = 15;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>RMG Lettings</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Affordable Student Accommodation in Manchester" />
<meta name="keywords"  content="student,homes,manchester,lettings,monty hall,montgomery house,enquire now,student accommodation,affordable student accomodation" />
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/common.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/themes_smoothness_jquery-ui.css" rel="stylesheet" type="text/css" media="screen" /> 
<link rel="stylesheet" href="../css/font-awesome.min.css">

<!--[if lte IE 8]>
<link href="/css/lte_ie8.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->

<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript">
	
	function send_form(){
		
		$.post("/application/index.php", 
		$("#form1").serialize(), 
		function(data){
			
			if( data['save_result'] == "success" ){
				location.href = "success.php";
			}
			else{
				location.href = "#";
				show_error(data['field_names'], data['save_msg']);
			}
		}, 
		"json");
	}
	
	$(document).ready(function(){

		$(".date").datepicker({dateFormat : "dd/mm/yy", changeYear : true, minDate : "+0D"});

		$(".dobdate").datepicker({dateFormat : "dd/mm/yy", changeYear : true});
		
		
		$("#home_address_country_input").change(
			function() {
				
				if($(this).val() == "1"){
					$("#home_address_postcode_label").html("Zip code");
				}
				else{
					$("#home_address_postcode_label").html("Postcode");
				}
				
				if($(this).val() == "15"){
					$("#home_address_postcode_label").html("Postcode");
				}
				else{
					$("#home_address_postcode_label").html("Postcode&nbsp;*");
				}
			}
		);
	
		$("#guarantor_address_country_input").change(
			function() {
					
				if($(this).val() == "1"){
					$("#guarantor_address_postcode_label").html("Zip code");
				}
				else{
					$("#guarantor_address_postcode_label").html("Postcode");
				}
				
				if($(this).val() == "15"){
					$("#guarantor_address_postcode_label").html("Postcode");
				}
				else{
					$("#guarantor_address_postcode_label").html("Postcode&nbsp;*");
				}
			}
		);
	
		$("#kin_address_country_input").change(
			function() {
					
				if($(this).val() == "1"){
					$("#kin_address_postcode_label").html("Zip code");
				}
				else{
					$("#kin_address_postcode_label").html("Postcode");
				}
				
				if($(this).val() == "15"){
					$("#kin_address_postcode_label").html("Postcode");
				}
				else{
					$("#kin_address_postcode_label").html("Postcode&nbsp;*");
				}
			}
		);
		
		$("#kin_required").change(function(){
			toggle_required();
		});
		
		$("#guarantor_required").change(function(){
			toggle_required();
		});
		
		$("#kin_same_address").change(function(){
			toggle_address('kin');
		});
		
		$("#guarantor_same_address").change(function(){
			toggle_address('guarantor');
		});
		
		$("#submit_button").bind("click", function(e){
			send_form();
			e.preventDefault();
		});
		
		toggle_address('kin');
		toggle_address('guarantor');
		
		toggle_required();
	   
	});
	
	function toggle_address(type){
		
		if( $("#" + type + "_same_address").is(':checked') ){
			$("." + type + "_address_block").hide();
		}
		else{
			$("." + type + "_address_block").show();
		}
	}
	
	function toggle_required(){
		
		if( $("#guarantor_required").is(':checked') ){
			
			$("#guarantor_block").show();

			$("#kin_required_block").show();
			
			if( $("#kin_required").is(':checked') ){
				$("#kin_block").hide();
			}else{
				$("#kin_block").show();
			}
		}
		else{
			$("#guarantor_block").hide();

			$("#kin_required_block").hide();
			$("#kin_block").show();

			$("#kin_required").removeAttr('checked');
		}
	}
	
	function show_error(field_names, message){
		
		if( $.isArray(field_names) ){
			for(r=0;r<field_names.length;r++){
				$('#'+field_names[r]).addClass('text-info');
			}
		}
		
		$('#error_msg p').html(message);
		$('#error_msg').show();
	}
	
</script>
<? require_once("../includes/analytics.php");?>
</head>
<body>
<? require_once("../includes/header.php");?>
        
            <div class="container paddtop90">
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
                	<div class="row">                
                    	<div class="bluefont fontbg">Online Application Form</div>
	                    <div id="error_msg" class="text-danger" style="display:none;">
	                        <p class="text-success"><?=$save_success?></p>
	                    </div>
                    
                    <p class="text-primary">Please complete the form below and click the <strong><em>Submit</em></strong> button at the bottom of this page. This will send your details to our lettings team who will process your application. Required fields are marked with (<span class="text-danger">*</span>)</p>
                    
                    <form id="form1" name="form1" class="form-horizontal" method="post">
        
                        <input value="s" name="a" id="a" type="hidden">
                        <input value="<?=$_REQUEST['code']?>" name="code" id="code" type="hidden">
                        <input value="<?=$_REQUEST['t']?>" name="t" id="t" type="hidden">
                        
                        <div class="rmg_bullet fontxl border-row">Your Details</div>
                        
                        <!-- Applicant Names -->
					
					        <div class="form-group fontmd">
					            <label for="first_name_input" id="first_name_label" class="col-xs-4   greyfont">First name&nbsp;<span class="text-danger">*</span></label>
					            <div class="col-xs-4">
					               <input type="text" id="first_name_input" name="first_name_input" class="form-control"  value="<?=$_REQUEST['first_name_input']?>">
					            </div>
					        </div>
					        
					        <div class="form-group fontmd">
					            <label for="middle_name_input" id="middle_name_label" class="col-xs-4   greyfont ">Middle name</label>
					            <div class="col-xs-4">
					               <input type="text" id="middle_name_input" name="middle_name_input" class="form-control" value="<?=$_REQUEST['middle_name_input']?>">
					            </div>
					        </div>
					        
					        <div class="form-group fontmd">
					            <label for="last_name_input" id="last_name_label" class="col-xs-4  greyfont ">Last name&nbsp;<span class="text-danger">*</span></label>
					            <div class="col-xs-4">
					               <input type="text" id="last_name_input" name="last_name_input" class="form-control" value="<?=$_REQUEST['last_name_input']?>">
					            </div>
					        </div>
					        
                            
                        <!-- Applicants address -->
                        <div class="form-group fontmd">
					            <label for="home_address_1_input" id="home_address_1_label" class="col-xs-4  greyfont ">Street address 1&nbsp;<span class="text-danger">*</span></label>
					            <div class="col-xs-6">
					               <input type="text" id="home_address_1_input" name="home_address_1_input" class="form-control" value="<?=$_REQUEST['home_address_1_input']?>">
					            </div>
					        </div>
					        
					        <div class="form-group fontmd">
					            <label for="home_address_2_input" id="home_address_2_label" class="col-xs-4  greyfont ">Street address 2</label>
					            <div class="col-xs-6">
					               <input type="text" id="home_address_2_input" name="home_address_2_input" class="form-control" value="<?=$_REQUEST['home_address_2_input']?>">
					            </div>
					        </div>
					        
					        
					        <div class="form-group fontmd">
					            <label for="home_address_3_input" id="home_address_3_label" class="col-xs-4  greyfont ">Street address 3</label>
					            <div class="col-xs-6">
					               <input type="text" id="home_address_3_input" name="home_address_3_input" class="form-control" value="<?=$_REQUEST['home_address_3_input']?>">
					            </div>
					        </div>
					        
					        <div class="form-group fontmd">
					            <label for="home_address_town_input" id="home_address_town_label" class="col-xs-4 greyfont ">Town/City&nbsp;<span class="text-danger">*</span></label>
					            <div class="col-xs-4">
					               <input type="text"  id="home_address_town_input" name="home_address_town_input" class="form-control" value="<?=$_REQUEST['home_address_town_input']?>">
					            </div>
					        </div>
                        
                        
                        
                        
                        <div class="form-group fontmd">
                            <label class="col-xs-4  greyfont " for="home_address_county_input" id="home_address_county_label">County</label>
                            <div class="col-xs-4">
                                <input type="text" id="home_address_county_input" name="home_address_county_input" class="form-control" value="<?=$_REQUEST['home_address_county_input']?>">
                            </div>
                        </div>
                        
                        <div class="form-group fontmd">
                            <label class="col-xs-4  greyfont " for="home_address_postcode_input" id="home_address_postcode_label">Postcode&nbsp;<span class="text-danger">*</span></label>
                            <div class="col-xs-4">
                                <input type="text" id="home_address_postcode_input" name="home_address_postcode_input" class="form-control" value="<?=$_REQUEST['home_address_postcode_input']?>">
                            </div>
                        </div>
                        <div class="form-group fontmd">
                            <label class="col-xs-4  greyfont" for="home_address_country_input" id="home_address_country_label">Country&nbsp;<span class="text-danger">*</span></label>
                            <div class="col-xs-4">
                                <select id="home_address_country_input" name="home_address_country_input" class="form-control">
                                    <?=$country->gen_country_list_select($_REQUEST['home_address_country_input'])?>
                                </select>
                            </div>
                        </div>
                                
                        <!-- Applicants additional details -->
                        <div class="form-group fontmd">
                           <label class="col-xs-4  greyfont" for="home_tel_1_input" id="home_tel_1_label">Tel. no. 1&nbsp;<span class="text-danger">*</span></label>
                            <div class="col-xs-4">
                                <input type="text" id="home_tel_1_input" name="home_tel_1_input" class="form-control" value="<?=$_REQUEST['home_tel_1_input']?>">
                            </div>
                        </div>
                        <div class="form-group fontmd">
                            <label class="col-xs-4  greyfont" for="home_tel_2_input" id="home_tel_2_label">Tel. no. 2</label>
                            <div class="col-xs-4">
                                <input type="text" id="home_tel_2_input" name="home_tel_2_input" class="form-control" value="<?=$_REQUEST['home_tel_2_input']?>">
                            </div>
                        </div>
                        <div class="form-group fontmd">
                            <label class="col-xs-4  greyfont" for="home_email_input" id="home_email_label">Email&nbsp;<span class="text-danger">*</span></label>
                            <div class="col-xs-4">
                                <input type="text" id="home_email_input" name="home_email_input" class="form-control" value="<?=$_REQUEST['home_email_input']?>">
                            </div>
                        </div>
                        <div class="form-group fontmd">
                            <label class="col-xs-4  greyfont" for="home_dob_input" id="home_dob_label">Date of birth&nbsp;<span class="text-danger">*</span></label>
                            <div class="col-xs-4">
                                <input type="text" id="home_dob_input" name="home_dob_input" class="form-control dobdate" maxlength="10" value="<?=$_REQUEST['home_dob_input']?>">
                            </div>
                        </div>
                        <div class="form-group fontmd">
                            <label class="col-xs-4  greyfont" for="home_gender_input" id="home_gender_label">Gender&nbsp;<span class="text-danger">*</span></label>
                            <div class="col-xs-4">
                                <select id="home_gender_input" name="home_gender_input" class="form-control">
                                    <option selected="selected">-- select --</option>
                                    <option value="M" <? if($_REQUEST['home_gender_input'] == "M"){?>selected="selected"<? }?>>Male</option>
                                    <option value="F" <? if($_REQUEST['home_gender_input'] == "F"){?>selected="selected"<? }?>>Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group fontmd">
                            <label class="col-xs-4  greyfont" for="home_nation_input" id="home_nation_label">Nationality&nbsp;<span class="text-danger">*</span></label>
                            <div class="col-xs-4">
                                <select id="home_nation_input" name="home_nation_input" class="form-control">
                                    <?=$nationality->gen_nat_list_select($_REQUEST['home_nation_input'])?>
                                </select>
                            </div>
                        </div>
                        
                        
                        <div class="rmg_bullet fontxl border-row">Course details</div>
                        <!-- Uni Info -->
                        <div class="form-group fontmd">
                            <label class="col-xs-4  greyfont" for="uni_name_input" id="uni_name_label">University/ College&nbsp;<span class="text-danger">*</span></label>
                            <div class="col-xs-6">
                                <input type="text" id="uni_name_input" name="uni_name_input" class="form-control" value="<?=$_REQUEST['uni_name_input']?>">
                            </div>
                        </div>
                        <div class="form-group fontmd">
                            <label class="col-xs-4  greyfont" for="uni_course_input" id="uni_course_label">Course&nbsp;<span class="text-danger">*</span></label>
                            <div class="col-xs-6">
                                <input type="text" id="uni_course_input" name="uni_course_input" class="form-control" value="<?=$_REQUEST['uni_course_input']?>">
                            </div>
                        </div>
                        <div class="form-group fontmd">
                            <label class="col-xs-4  greyfont" for="uni_student_id_input" id="uni_student_id_label">Student ID number</label>
                            <div class="col-xs-4">
                                <input type="text" id="uni_student_id_input" name="uni_student_id_input" class="form-control" value="<?=$_REQUEST['uni_student_id_input']?>">
                            </div>
                        </div>    
                        
                        <!-- About your stay -->
                        <div class="form-group fontmd">
                            <label class="col-xs-4  greyfont" for="stay_start_date_input" id="stay_start_date_label">Move-in date&nbsp;<span class="text-danger">*</span></label>
                            <div class="col-xs-4">
                                <input type="text" id="stay_start_date_input" name="stay_start_date_input" class="form-control date" maxlength="10" value="<?=$_REQUEST['stay_start_date_input']?>"> 
                            </div>
                        </div>
                        <div class="form-group fontmd">
                            <label class="col-xs-4  greyfont" for="stay_end_date_input" id="stay_end_date_label">Move-out date&nbsp;<span class="text-danger">*</span></label>
                            <div class="col-xs-4">
                                <input type="text" id="stay_end_date_input" name="stay_end_date_input" class="form-control date" maxlength="10" value="<?=$_REQUEST['stay_end_date_input']?>">
                            </div>
                        </div>
                        <div class="form-group fontmd">
                            <label class="col-xs-4  greyfont" for="notes_input">Additional comments</label>
                            <div class="col-xs-6">
                                <p><small>All of our properties include floors that are specific to gender, or have disabled access rooms. If you require specific room allocation to a single sex floor or access to a disabled room then please specify this below.</small></p>
                                <textarea id="notes_input" name="notes_input" class="form-control" rows="5"><?=$_REQUEST['notes_input']?></textarea>
                            </div>
                        </div>
        
            			<div class="rmg_bullet fontxl border-row">Guarantor Details</div>
                        <p class="lbluefont"><small>Details for the person that will act as a Guarantor.</small></p>
                        
                        
                        <div class="form-group fontmd" style="margin-bottom:0px;margin-top:30px;margin-left:3px;">
                            <div class="alert alert-info">
                                <label class="checkbox" for="guarantor_required">
                                    <input type="checkbox" id="guarantor_required" name="guarantor_required" value="Y" <? if($_REQUEST['guarantor_required'] == "Y"){?>checked<? }?>>&nbsp;&nbsp;A Guarantor is required
                                </label>
                            </div>
                        </div>
						<div id="guarantor_block">      
	                        <!-- Guarantor -->
	                        <div class="form-group fontmd">
	                            <label class="col-xs-4  greyfont" for="guarantor_first_name_input" id="guarantor_first_name_label">First name&nbsp;<span class="text-danger">*</span></label>
	                            <div class="col-xs-4">
	                                <input type="text" id="guarantor_first_name_input" name="guarantor_first_name_input" class="form-control" value="<?=$_REQUEST['guarantor_first_name_input']?>">
	                            </div>
	                        </div>
	                        <div class="form-group fontmd">
	                            <label class="col-xs-4  greyfont" for="guarantor_last_name_input" id="guarantor_last_name_label">Last name&nbsp;<span class="text-danger">*</span></label>
	                            <div class="col-xs-4">
	                                <input type="text" id="guarantor_last_name_input" name="guarantor_last_name_input" class="form-control" value="<?=$_REQUEST['guarantor_last_name_input']?>">
	                            </div>
	                        </div>
	                        <div class="form-group fontmd">
	                            <label class="col-xs-4  greyfont" for="guarantor_relationship_input" id="guarantor_relationship_label">Relationship to Applicant&nbsp;<span class="text-danger">*</span></label>
	                            <div class="col-xs-4">
	                                <input type="text" id="guarantor_relationship_input" name="guarantor_relationship_input" class="form-control" value="<?=$_REQUEST['guarantor_relationship_input']?>">
	                            </div>
	                        </div>
	                        
	                        <div class="form-group fontmd" style="margin-bottom:0px;margin-top:30px;margin-left:3px;">
	                            <div class="alert alert-info">
	                                <label class="checkbox" for="guarantor_same_address">
	                                    <input type="checkbox" id="guarantor_same_address" name="guarantor_same_address" value="Y" <? if($_REQUEST['guarantor_same_address'] == "Y"){?>checked<? }?>>&nbsp;&nbsp;Guarantor address is the same as the applicant above 
	                                </label>
	                            </div>
	                        </div>
	                        
	                        <div class="form-group fontmd guarantor_address_block">
	                            <label class="col-xs-4  greyfont" for="guarantor_address_1_input" id="guarantor_address_1_label">Street address 1&nbsp;<span class="text-danger">*</span></label>
	                            <div class="col-xs-6">
	                                <input type="text" id="guarantor_address_1_input" name="guarantor_address_1_input" class="form-control" value="<?=$_REQUEST['guarantor_address_1_input']?>">
	                            </div>
	                        </div>
	                        <div class="form-group fontmd guarantor_address_block">
	                            <label class="col-xs-4  greyfont" for="guarantor_address_2_input" id="guarantor_address_2_label">Street address 2</label>
	                            <div class="col-xs-6">
	                                <input type="text" id="guarantor_address_2_input" name="guarantor_address_2_input" class="form-control" value="<?=$_REQUEST['guarantor_address_2_input']?>">
	                            </div>
	                        </div>
	                        <div class="form-group fontmd guarantor_address_block">
	                            <label class="col-xs-4  greyfont" for="guarantor_address_3_input" id="guarantor_address_3_label">Street address 3</label>
	                            <div class="col-xs-6">
	                                <input type="text" id="guarantor_address_3_input" name="guarantor_address_3_input" class="form-control" value="<?=$_REQUEST['guarantor_address_3_input']?>">
	                            </div>
	                        </div>
	                        <div class="form-group fontmd guarantor_address_block">
	                            <label class="col-xs-4  greyfont" for="guarantor_address_town_input" id="guarantor_address_town_label">Town/City&nbsp;<span class="text-danger">*</span></label>
	                            <div class="col-xs-4">
	                                <input type="text" id="guarantor_address_town_input" name="guarantor_address_town_input" class="form-control" value="<?=$_REQUEST['guarantor_address_town_input']?>">
	                            </div>
	                        </div>
	                        <div class="form-group fontmd guarantor_address_block">
	                            <label class="col-xs-4  greyfont" for="guarantor_address_county_input" id="guarantor_address_county_label">County</label>
	                            <div class="col-xs-4">
	                                <input type="text" id="guarantor_address_county_input" name="guarantor_address_county_input" class="form-control" value="<?=$_REQUEST['guarantor_address_county_input']?>">
	                            </div>
	                        </div>
	                        <div class="form-group fontmd guarantor_address_block">
	                            <label class="col-xs-4  greyfont" for="guarantor_address_postcode_input" id="guarantor_address_postcode_label">Postcode&nbsp;<span class="text-danger">*</span></label>
	                            <div class="col-xs-4">
	                                <input type="text" id="guarantor_address_postcode_input" name="guarantor_address_postcode_input" class="form-control" value="<?=$_REQUEST['guarantor_address_postcode_input']?>">
	                            </div>
	                        </div>
	                        <div class="form-group fontmd guarantor_address_block">
	                            <label class="col-xs-4  greyfont" for="guarantor_address_country_input" id="guarantor_address_country_label">Country&nbsp;<span class="text-danger">*</span></label>
	                            <div class="col-xs-4">
	                                <select id="guarantor_address_country_input" name="guarantor_address_country_input" class="form-control">
	                                    <?=$country->gen_country_list_select($_REQUEST['guarantor_address_country_input'])?>
	                                </select> 
	                            </div>
	                        </div>
	                        <div class="form-group fontmd">
	                            <label class="col-xs-4  greyfont" for="guarantor_email_input" id="guarantor_email_label">Email Address</label>
	                            <div class="col-xs-4">
	                                <input type="text" id="guarantor_email_input" name="guarantor_email_input" class="form-control" value="<?=$_REQUEST['guarantor_email_input']?>">
	                            </div>
	                        </div>
	                        <div class="form-group fontmd">
	                            <label class="col-xs-4  greyfont" for="guarantor_tel_1_input" id="guarantor_tel_1_label">Tel. no. 1</label>
	                            <div class="col-xs-4">
	                                <input type="text" id="guarantor_tel_1_input" name="guarantor_tel_1_input" class="form-control" value="<?=$_REQUEST['guarantor_tel_1_input']?>">
	                            </div>
	                        </div>
	                        <div class="form-group fontmd">
	                            <label class="col-xs-4  greyfont" for="guarantor_tel_2_input" id="guarantor_tel_2_label">Tel. no. 2</label>
	                            <div class="col-xs-4">
	                                <input type="text" id="guarantor_tel_2_input" name="guarantor_tel_2_input" class="form-control" value="<?=$_REQUEST['guarantor_tel_2_input']?>">
	                            </div>
	                        </div>
						</div>
            
            		<div class="rmg_bullet fontxl border-row">Next of Kin</div>
                        <p class="lbluefont"><small>Details of your nearest relative, or somebody that you would want us to contact in the case of an emergency.</small></p>
                        
                        <div id="kin_required_block" class="form-group fontmd" style="margin-bottom:0px;margin-top:30px;margin-left:3px;">
                            <div class="alert alert-info">
                                <label class="checkbox" for="kin_required">
                                    <input type="checkbox" id="kin_required" name="kin_required" value="Y" <? if($_REQUEST['kin_required'] == "Y"){?>checked<? }?>>&nbsp;&nbsp;Next of Kin is the same as Guarantor
                                </label>
                            </div>
                        </div>
						<div id="kin_block"> 
                            
	                        <!-- Next of Kin -->
	                        <div class="form-group fontmd">
	                            <label class="col-xs-4  greyfont" for="kin_first_name_input" id="kin_first_name_label">First name&nbsp;<span class="text-danger">*</span></label>
	                            <div class="col-xs-4">
	                                <input type="text" id="kin_first_name_input" name="kin_first_name_input" class="form-control" value="<?=$_REQUEST['kin_first_name_input']?>">
	                            </div>
	                        </div>
	                        <div class="form-group fontmd">
	                            <label class="col-xs-4  greyfont" for="kin_last_name_input" id="kin_last_name_label">Last name&nbsp;<span class="text-danger">*</span></label>
	                            <div class="col-xs-4">
	                                <input type="text" id="kin_last_name_input" name="kin_last_name_input" class="form-control" value="<?=$_REQUEST['kin_last_name_input']?>">
	                            </div>
	                        </div>
	                        
	                        <div class="form-group fontmd" style="margin-bottom:0px;margin-top:30px;margin-left:3px;">
	                            <div class="alert alert-info">
	                                <label class="checkbox" for="kin_same_address">
	                                    <input type="checkbox" id="kin_same_address" name="kin_same_address" value="Y" <? if($_REQUEST['kin_same_address'] == "Y"){?>checked<? }?>>&nbsp;&nbsp;Next of Kin address is the same as the applicant above 
	                                </label>
	                            </div>
	                        </div>
	                        
	                        <div class="form-group fontmd kin_address_block">
	                            <label class="col-xs-4  greyfont" for="kin_address_1_input" id="kin_address_1_label">Street address 1&nbsp;<span class="text-danger">*</span></label>
	                            <div class="col-xs-6">
	                                <input type="text" id="kin_address_1_input" name="kin_address_1_input" class="form-control" value="<?=$_REQUEST['kin_address_1_input']?>">
	                            </div>
	                        </div>
	                        <div class="form-group fontmd kin_address_block">
	                            <label class="col-xs-4  greyfont" for="kin_address_2_input" id="kin_address_2_label">Street address 2</label>
	                            <div class="col-xs-6">
	                                <input type="text" id="kin_address_2_input" name="kin_address_2_input" class="form-control" value="<?=$_REQUEST['kin_address_2_input']?>">
	                            </div>
	                        </div>
	                        <div class="form-group fontmd kin_address_block">
	                            <label class="col-xs-4  greyfont" for="kin_address_3_input" id="kin_address_3_label">Street address 3</label>
	                            <div class="col-xs-6">
	                                <input type="text" id="kin_address_3_input" name="kin_address_3_input" class="form-control" value="<?=$_REQUEST['kin_address_3_input']?>">
	                            </div>
	                        </div>
	                        <div class="form-group fontmd kin_address_block">
	                            <label class="col-xs-4  greyfont" for="kin_address_town_input" id="kin_address_town_label">Town/City&nbsp;<span class="text-danger">*</span></label>
	                            <div class="col-xs-4">
	                                <input type="text" id="kin_address_town_input" name="kin_address_town_input" class="form-control" value="<?=$_REQUEST['kin_address_town_input']?>">
	                            </div>
	                        </div>
	                        <div class="form-group fontmd kin_address_block">
	                            <label class="col-xs-4  greyfont" for="kin_address_county_input" id="kin_address_county_label">County</label>
	                            <div class="col-xs-4">
	                                <input type="text" id="kin_address_county_input" name="kin_address_county_input" class="form-control" value="<?=$_REQUEST['kin_address_county_input']?>">
	                            </div>
	                        </div>
	                        <div class="form-group fontmd kin_address_block">
	                            <label class="col-xs-4  greyfont" for="kin_address_postcode_input" id="kin_address_postcode_label">Postcode&nbsp;<span class="text-danger">*</span></label>
	                            <div class="col-xs-4">
	                                <input type="text" id="kin_address_postcode_input" name="kin_address_postcode_input" class="form-control" value="<?=$_REQUEST['kin_address_postcode_input']?>">
	                            </div>
	                        </div>
	                        <div class="form-group fontmd kin_address_block">
	                            <label class="col-xs-4  greyfont" for="kin_address_country_input" id="kin_address_country_label">Country&nbsp;<span class="text-danger">*</span></label>
	                            <div class="col-xs-4">
	                                <select id="kin_address_country_input" name="kin_address_country_input" class="form-control">
	                                    <?=$country->gen_country_list_select($_REQUEST['kin_address_country_input'])?>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="form-group fontmd">
	                            <label class="col-xs-4  greyfont" for="kin_tel_1_input" id="kin_tel_1_label">Tel. no. 1</label>
	                            <div class="col-xs-4">
	                                <input type="text" id="kin_tel_1_input" name="kin_tel_1_input" class="form-control" value="<?=$_REQUEST['kin_tel_1_input']?>">
	                            </div>
	                        </div>
	                        <div class="form-group fontmd">
	                            <label class="col-xs-4  greyfont" for="kin_tel_2_input" id="kin_tel_2_label">Tel. no. 2</label>
	                            <div class="col-xs-4">
	                                <input type="text" id="kin_tel_2_input" name="kin_tel_2_input" class="form-control" value="<?=$_REQUEST['kin_tel_2_input']?>">
	                            </div>
	                        </div>
                        </div>
                        
                        <div class="form-group">					
				            <div class="col-xs-offset-4 col-xs-10">					
				                <button id="submit_button" class="btn btn-primary">Submit</button>					
				            </div>					
				        </div>
                    
                    </form>
                    
                </div>
            </div>
        
    </div>
<?php include_once '../includes/footer.php';?>

</body>
</html>
